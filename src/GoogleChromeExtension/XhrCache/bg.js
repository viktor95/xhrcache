DBG = 1;
log('- BGNEW:');

function log(msg) {
    console.log(msg);
}

var _iDB = window.indexedDB || window.mozIndexedDB || window.webkitIndexedDB || window.msIndexedDB;
var _db;
var _tab, _atab = [], _tabList = {};
var _mode = {value: 'MODE_PLAY', id: 'MdOff'}, _filter = {domain: 'http*', request: '*'},
//var _mode = {value: 'MODE_REC', id: 'MdOff'}, _filter = {domain: '*spark*', request: '*api*'},
//var _mode = {value: 'MODE_PLAY', id: 'MdOff'}, _filter = {domain: 'http*', request: '*api*'},
//var _mode = {value: 'MODE_OFF', id: 'MdOff'}, _filter = {domain: 'http*', request: '*api*'},
//var _mode = {value: 'MODE_OFF', id: 'MdOff'}, _filter = {domain: 'http*sparkas*', request: '*api*'},
//todo
    _UID = '505085893';
//var _callUrl = '*api*', _docUrl = 'http*';

(function() {
    _db = new DB('CacheDB');
    var isNew = true;
    _db.up().then(function () {
//        this.clear('cache');
        this.storeUp([
            {store: 'cache', col: [
                {name: 'uid', key: 'uid'},
                {name: 'Request', key: 'req'},
                {name: 'Response', key: 'res'},
                {name: 'verb', key: 'verb'},
                {name: 'hdreq', key: 'hdreq'},
                {name: 'hdres', key: 'hdres'},
                {name: 'dtin', key: 'dtin'},
                {name: 'dtout', key: 'dtout'},
                {name: 'miss', key: 'miss'},
            ], replace: isNew}
        ], function () {    log('bg.storeUp_done:'+_dDB.isOpen());
            _db.fetch('cache', {'Request': 'CURRENT_USER'}, function(data) { //console.dir(data);
                if (data && data.res)
                    _UID = data.res;
            });
        });
/*        _db.fetch('cache', {'Request': 'CURRENT_USER'}, function(data) { //console.dir(data);
            if (data)
                _UID = data.res;
        });*/
//        this.tableUp('cache');
//        alert(123);
    });
})();

if (chrome.runtime.onMessage) { log('bg_listen:');
//* from active tab
    chrome.runtime.onMessage.addListener(function (msg, sender, sendResponse) { log('bg.Message:'+msg.tab+"::"+msg.subject);

        switch (msg.subject) {
            case 'showPageAction':
//** activate pageAction
                _tab = sender.tab.id;
                if (_atab.indexOf(sender.tab.id) < 0) {
                    _atab.push(sender.tab.id);
                }
//                chrome.browserAction.show(sender.tab.id);
//                chrome.pageAction.show(sender.tab.id);
                break;
            case 'UID_SET': log('bg.UID_SET:'+JSON.stringify(msg));
                //console.log(msg.body);
                //console.log(JSON.parse(msg.body));
                _UID = JSON.parse(msg.body).uid;
                _db.put('cache', { 'req': 'CURRENT_USER', 'res': _UID }, true);
/*                this.put('cache', item).then(function() {   log('bg.TO_CACHE.DONE:'+_tabList['BLANK']);
                    send2tab(_tabList['BLANK'], 'TO_LIST', item);
                });*/
                break;
            case 'ITEM_DEL':
                _db.del('cache', 'Request', msg.body.req).then(function() {
//* update
                    sendResponse({body: 'OK'});
/*                    _db.get('cache').then(function(res) {   log('bg.ITEM_DEL_REFR:' + res);
                        console.dir(res);
                        send2tab('BLANK', 'CACHE_GET', res);
                    });*/
                });
                break;
            case 'SET_FILTERS':
                _filter = msg.body;
                break;
            case 'FILTER_UP':   lo('bg.FILTER_UP:'+_atab);
                _filter = msg.body;
                send2tab(null, 'FILTER_UP', _filter);
                break;
            case 'FR_CACHE':    lo('bg.FR_CACHE:'+_tab);
                var res = JSON.parse(msg.body);
                console.dir(res);
                var cb = function(data, url) {   log('bg.FR_CACHE_totab:'+_tab+"::"+url);
                    console.dir(data);
                    //data.url = urlTrim(res.finalUrl);
                    chrome.tabs.sendMessage(_tab, {
                        from: 'bg',
                        subject: 'B2C_FR_CACHE',
                        body: data,
                        url: url
                    });
                };
                var url = urlTrim(res.finalUrl);
                        lo('bg.FR_CACHE2:'+url+"::"+_UID);
                var cbYES = function (data) {   log('-- fromCache_YES:'+url);//+"::"+this._out);
                    console.dir(data);
                    //data = this._out;
//                    console.dir(this._out);
                    if (!data)
                        data = {res: null};
                    if (cb)
                        cb.call(this, data.res, url);
                };
                var cbNO = function () {   log('-- fromCache_NOT:'+url);
                    /*                    send2tab(_tab, 'INP2CACHE', res.finalUrl);
                     return;
                     if (cb)
                     cb.call(this, null);*/
                    if (cb)
                        cb.call(this, null, url);
/*                    if (!this._out)
                        this._out = {res: null};
                    if (cb)
                        cb.call(this, this._out.res, url);*/
                };
                _db.exists('cache', { req: url , uid: _UID, miss: 0}, cbYES, cbNO);//.then(cbYES).else(cbNO);
                break;
            case 'CACHE_PUSH':  log('bg.CACHE_PUSH:' + JSON.stringify(msg.body));
                break;
            case 'TO_CACHE':
                var data = msg.body;
                var res = JSON.parse(msg.body.text);
//* strip identifier
                var url = urlTrim(res.finalUrl);
                var miss = (data.miss || 0);
                if (url.match('/status?') || url.match('/state?')) {
                    log('??SS:' + url+"::"+url.match('/status')+"::"+url.match('/state'));// +"::"+msg.body.text);
                }
//                    break;
                        log('??? bg.TO_CACHE:'+url+"::"+_UID+"::"+ msg.body.length+"::"+miss);//+"::"+res.text.length);
                        console.dir(data);
                var item = {
                    'uid': _UID,
                    'req': url,
                    'res': res.text,
                    'verb': data.verb,
                    'hdreq': data.hdreq,
                    'hdres': data.hdres,
                    'dtin': new Date(),
                    'miss': miss,
                };
                        console.dir(item);
//                        break;
                var cbNO = function () {
                    /*                                if (item.res && item.res.length) {
                     var obj = JSON.parse(item.res);
                     if (obj.collection) {
                     var arr = obj.collection;
                     for (var i in arr) {
                     if (arr[i].sender)
                     arr[i].sender.bic = 'BIC_MOCK';
                     }
                     }
                     item.res = JSON.stringify(obj);
                     }*/
//!!                    if (item.res && item.res.length > 2)
//!!                        item.miss = 0;
                    this.put('cache', item, true).then(function() {   //log('bg.TO_CACHE.DONE:'+_tabList['BLANK']);
                        log('+++ bg.TO_CACHE:'+url+"::"+item.miss);
                        if (item.res && item.res.length) {
                            try {
                                console.dir(JSON.parse(item.res));
                            } catch (ex) {
                                log('EX_NO_JSON:');
                            }
                        }
                        send2tab(_tabList['BLANK'], 'TO_LIST', item);
                    });
                };
                _db.exists('cache', { 'req': url, 'uid': _UID, 'miss': 0 }, null, cbNO);//.else();
                break;
            case 'NEW_TAB':
                chrome.tabs.create({url: msg.body + '?name=' + msg.tab, active: true, selected: true}, function(tab) {
                    _tabList[msg.tab] = tab.id;
                });
                break;
            case 'CACHE_GET':   log('bg.CACHE_GET:' +_tabList[msg.tab]);
                sendResponse(_filter);
                chrome.browserAction.disable(_tabList[msg.tab]);
                _db.fetch('cache', {miss: 0}, function(res) {   log('bg.CACHE_GET:GOT:' + res.length);
//                _db.get('cache').then(function(res) {   log('bg.CACHE_GET:GOT:' + res.length);
                            console.dir(res);
//                            return;
                    var lst = [];
                    for (var i in res) {
                        lst.push(db2ui(res[i]));
                    }
                            log('bg.CACHE_GET_TRANS:');
                            console.dir(lst);
                    send(msg.tab, {subject: msg.subject, body: lst});
//                    send(msg.tab, {subject: msg.subject, body: res});
                });
                break;
            case 'missed_cache_get_message_subject':
                log('bg.missed_cache_get_message_subject'+msg.tab);
                _db.fetch('cache', {miss: 1}, function (res) { log('missed_cache_get_message_subject_GOT:'+res.length);
                    console.dir(res);
                    var lst = [];
                    for (var i in res) {
                        lst.push(db2miss(res[i]));
                    }
                    send(msg.tab, {subject: msg.subject, body: lst});
                });
                break;
            case 'SAVE_CACHE_EDIT':
                log('bg.SAVE_CACHE_EDIT:');
                var dbData = ui2db(msg.body);
//                        console.dir(msg.body);
                        console.dir(dbData);
                _db.put('cache', dbData, true).then(function() {  log('bg.SAVE_CACHE_EDIT1:'+msg.tab);
                    send(msg.tab, {subject: 'SAVE_CACHE_EDIT', body: 'OK'});
                });
                break;
            case 'CACHE_EXPORT':
                _db.get('cache').then(function(res) {   log('bg.CACHE_EXPORT:GOT:');
                    console.dir(res);
                    send(msg.tab, {subject: msg.subject, body: res});
                });
                break;
            case 'CACHE_IMPORT':
//                log('bg.CACHE_IMPORT:'+msg.body.length);return;
                _db.clear('cache').then(function() {
                    arr2cache(msg.body, function() {
                        send(msg.tab, {subject: 'CACHE_IMPORT'});
/*                        _db.get('cache').then(function(res) {   log('bg.CACHE_IMPORT:CACHE_GOT:' + res);
                            console.dir(res);
                            send(msg.tab, {subject: 'CACHE_IMPORT'});
                        });*/
                    });
                });
                break;
            case 'MODE_OFF':
                _mode = {value: msg.subject, id: msg.body.id};
                send2tab(_tab, msg.subject);
                chrome.browserAction.setIcon({path : {"38": "img/hat1_38.png"}});
//                chrome.pageAction.setIcon({tabId: _tab, path : {"38": "img/hat1_38.png"}});
                break;
            case 'MODE_EDIT':
                _mode = {value: msg.subject, id: msg.body.id};
                break;
            case 'MODE_PLAY':
                _mode = {value: msg.subject, id: msg.body.id};
                send2tab(_tab, msg.subject, {template: $('#PopOuter').html()});
                chrome.browserAction.setIcon({path : {"38": "img/play1_38.png"}});
//                chrome.pageAction.setIcon({path : {"38": "img/play1_38.png"}});
//                chrome.pageAction.setIcon({tabId: _tab, path : {"38": "img/play1_38.png"}});
                break;
            case 'MODE_REC':
                _mode = {value: msg.subject, id: msg.body.id};
                send2tab(_tab, msg.subject, {template: $('#PopOuter').html()});
                chrome.browserAction.setIcon({path : {"38": "img/rec1_38.png"}});
//                chrome.pageAction.setIcon({tabId: _tab, path : {"38": "img/rec1_38.png"}});
                break;
            case 'GET_MODE':    log('bg.GET_MODE:');//+JSON.stringify_mode);
                sendResponse({mode: _mode, filter: _filter});
                break;
            case 'MODE_ON': log('bg.MODE_ON:'+_filter+"::"+JSON.stringify(_mode));
                send2tab(_tab, _mode.value);//, {template: $('#PopOuter').html()});
                break;
            case 'GET_INJECT':
                sendResponse({template: $('#PopOuter').html()});
                if (_filter)
                    send2tab(_tab, 'FILTER_UP', _filter);
//                if (!_mode)
//                    return;
                send2tab(_tab, _mode.value, {template: $('#PopOuter').html()});
                break;
        }
    })
}

function urlTrim(url) {

    url = url.split('_=');
    if (url.length > 0 && !isNaN(url[url.length - 1]))
        url.pop();
    url = url.join('_=');

    var reDate = /from=.+&/g;
    url = url.replace(reDate, '');

    return url;
}

function arr2cache(arr, cb) {   //log('arr2cache:'+arr.length);

    var item = arr.shift();
    if (!item) {    log('arr2cache_END:');
        if (cb)
            cb.call();
        return;
    }
    _db.exists('cache', { 'req': item.req }).else(function () {
        this.put('cache', { 'req': item.req, 'res': item.res }).then(arr2cache(arr, cb));
    });
}

function db2miss(rec) {
    return {
        url: rec.req,
        absoluteUrl: rec.req,
        method: rec.verb,
        clientIp: "0:0:0:0:0:0:0:1",
        headers: {
            'Accept': rec.hdreq['Accept'],
            'Accept-Language': rec.hdreq['Client-Accept-Language'],
        },
        cookies: {},
        "browserProxyRequest": false,
        "bodyAsBase64": "",
        "body": "",
        "loggedDate": rec.dtin,
        "loggedDateString": rec.dtin
    };
}

function db2ui(dbRec) {
//??
//            dbRec.verb = 'GET';
            dbRec.status = 200;
            dbRec.hdReq = //fromKeys(dbRec.hdReq);
            {
                "Authorization": {
                    "equalTo": //dbRec.hdreq['Authorization']
                               '' + _UID
                },
                "Accept": {
                    "equalTo": dbRec.hdreq['Accept']
                               // 'application/json'
                }
            };
            dbRec.hdRes = //fromKeys(dbRec.hdRes);
            {
                "Access-Control-Allow-Credentials": "true",
                "Access-Control-Allow-Methods": "PUT, GET, POST, DELETE, OPTIONS",
                "Access-Control-Allow-Headers": "Accept, Accept-Language, Authorization, Content-Type, X-REQUEST-ID, X-GEORGE-API-VERSION, X-GEORGE-USER, X-ebsapi-Authentication, X-ebsapi-Accept, Client-Accept-Language",
                "Access-Control-Max-Age": "3600",
                "Content-Type": dbRec.hdres['content-type'],
                                //"application/json",
                "Content-Length": '' + (dbRec.res? dbRec.res.length: 0),
                "Authorization": '' + _UID
            };
    return new Record(dbRec);
}

function Record(rec) {

    this.id = rec.id;// rec.req;
    this.uuid = rec.uid;//this.id;
    this.request = {
        url: rec.req,
        method: rec.verb,
        headers: rec.hdReq,
    };
    this.response = {
        status: rec.status,
        body: rec.res,
        headers: rec.hdRes,
    };
}

function ui2db(uiRec)
{
    var miss = (uiRec.response.body? 0 : 1);
    var result = {
        id: uiRec.id,
        uid: _UID,
        req : uiRec.request.url,
        res: uiRec.response.body,
        verb : uiRec.request.method,
        hdReq : uiRec.request.headers,
        hdRes: uiRec.response.headers,
        miss: miss,
        status: uiRec.response.status,
    };
    return result;
}

/*chrome.pageAction.onClicked.addListener(function(tab, msg) {

    _tab = tab.id;
//* inject HTML template
    chrome.tabs.sendMessage(tab.id, {
        name: 'pop',
        template: $('#PopOuter').html(),
    });
});*/


function fromKeys(obj) {

    var tobj = {};
    var key = Object.keys(obj);
    for (var i in keys) {
        tobj[key[i]] = obj[key[i]];
    }
    return tobj;
}

function send(tabName, msg) {

    chrome.tabs.sendMessage(_tabList[tabName], msg);
}