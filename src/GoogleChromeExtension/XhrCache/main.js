/**
 * Created by X on 11/22/2015.
 */

log('MAIN:'+document.location.href+"::"+(this == top));

var _mode;

if (this == top) {
//* activate pageAction
    chrome.runtime.sendMessage({
        from: 'content',
        subject: 'showPageAction'
    });

    send('GET_INJECT', null, function(res) {   log('main.GET_INJECT.OUT:');//+JSON.stringify(res));
        if (res.template) {
            $('#PopOuter').detach();
            $('BODY').append('<div id="PopOuter">' + res.template + '</div>').promise().done(function () {
                setUp();
            });
        }
    });

    send('GET_MODE', null, function(res) { log('main.GET_MODE:OUT:'+JSON.stringify(res)+"::"+res.mode.value);
        _mode = res;
//        window.postMessage({from: 'CONTENT', type: 'MODE_SET', text: res}, '*');
//        _mode = res.mode;
//        _filter = res.filter;
    });

    chrome.runtime.onMessage.addListener(function (msg, from, sendResp) {   log('main.onMessage:' + msg.tab + "::" + msg.subject);//+JSON.stringify(msg));

        if (window != top)
            return;
//        log("MAIN.onMessage:" + PopUp+"::"+msg.name+"::"+msg.from+"::"+JSON.stringify(from));//+"::"+msg.data);//+"::"+JSON.stringify(msg.data));
        switch (msg.name) {
            case 'pop':
//* inject page control
                if ($('#PopOuter').length) {
                    cacheOn(false);
                    return $('#PopOuter').detach();
                }
                cacheOn(true);
                $('BODY').append('<div id="PopOuter">' + msg.template + '</div>').promise().done(function () {
//** set page actions
                    setUp();
                });
                break;
        }
        switch (msg.subject) {
        case 'INP2CACHE':   log('main.INP2CACHE:');
            $('#UrlValue').html(msg.body);
            $('#FMiss').removeClass('blank');
            break;
        case 'B2C_FR_CACHE':
//* get JSON data
/*                        if (msg.body) {
                            msg.body = msg.body.replace(/Dipl/g, 'WWWWWW');
                            log('main.B2C_FR_CACHE:');// +msg.body.req+"<<");//+"::"+msg.body+"::"+document.getElementById('P2ASocket'));
                            console.dir(msg);
                        }*/
                log('main.B2C_FR_CACHE:'+msg.url);// +msg.body.req+"<<");//+"::"+msg.body+"::"+document.getElementById('P2ASocket'));
                console.dir(msg);
/*            var data = {};
            if (msg.body) {
//                console.dir(JSON.parse(msg.body));
                data = JSON.parse(msg.body);
                data.url = msg.url;
                //console.dir(data);
            }
            data = JSON.stringify(data);*/
/*            if (!msg.body || !msg.body.length || msg.body == '[]') {
                $('#FMiss').removeClass('blank');
                return;
            }*/
            window.postMessage({from: 'CONTENT', type: 'FR_PLUGIN', text: msg.body, url: msg.url}, '*');
/*            var element = document.getElementById('P2ASocket');
            element.value = data;//msg.body;
            var evt = document.createEvent("HTMLEvents");
            evt.initEvent("change", false, true);
            element.dispatchEvent(evt);*/
//            runScript('fromPlugin(\''+msg.body+'\');');
            break;
        case 'MODE_REC':
            cacheOn(true);
            cacheUp(false);
            return;
            if ($('#PopOuter').length) {
                cacheUp(false);
                $('#PopOuter').removeClass('off')
                return $('#FPost').removeClass('off');
            }
            cacheOn(true);
            $('BODY').append('<div id="PopOuter">' + msg.body.template + '</div>').promise().done(function () {
                setUp();
            });
            break;
        case 'MODE_PLAY':
            cacheOn(true);
            cacheUp(true);
            return;
            if ($('#PopOuter').length) {    //log('main.MODE_PLAY:'+$('#PopOuter').length);
                cacheUp(true);
                $('#PopOuter').addClass('off');
                return $('#FPost').addClass('off');
            }
            cacheOn(true);
            $('BODY').append('<div id="PopOuter" class="off">' + msg.body.template + '</div>').promise().done(function () {
                $('#FPost').addClass('off');
                setUp();
            });
            break;
        case 'MODE_OFF':
            cacheOn(false);
//            cacheUp(false);
            return;
            if ($('#PopOuter').length) {
                cacheOn(false);
                return $('#PopOuter').detach();
            }
            break;
        default:
            react(msg);
            break;
        }
    });
}
