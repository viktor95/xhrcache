/**
 * Created by X on 11/9/2016.
 */
DBG = 1;
log('= BLANK:' + document.location.href);

var _tabName;
var _curReq;
var editor;
var items = [], _curItem;


_tabName = 'BLANK';
send('CACHE_GET', null);

var urlListItemSelector = '#ListReq .list-group-item';


(function () {

    var contentHeight = $(window).height() - 180;
    $("#urlList").height(contentHeight)
    $("#jsonEditor").height(contentHeight);

})();


function createUrlList(urls) {
    var cnt = '';
    for (var i in urls) {
        cnt += '<a href="#" class="list-group-item" rowId="' + i + '" id="ReqRow_' + i + '"><i class="fa fa-trash delete" title="Delete"></i><span class="url value_url">' + urls[i].req + '</span></div>';
    }

    $('#ListReq').html(cnt).promise().done(function () {
        bindUrlList();
        var i = 0;
        $(urlListItemSelector).each(function () {
            if (!urls[i])
                return;
            $(this).attr('datares', urls[i].res);
            i++;
        });
    });
}

function bindUrlList() {
    $('.delete').on('click', function () {
        var id = $(this).parent().attr('rowId');
        log('bindUrlList.click:' + id + "::" + _curItem);
        var req = $('#ReqRow_' + id).children('.value_url').html();
        if (!confirm('CONFIRM_DELETE: ' + req))
            return;
        send('ITEM_DEL', { req: req }, function (res) {
            log('bindUrlList.del_DONE:' + JSON.stringify(res));
            if (res == 'OK' || true) {
                send('CACHE_GET', null);
            }
        });
        //        alert('delete - ' + id + $('#ReqRow_' + id).children('.value_url').html());
    });

    $(urlListItemSelector).on('click', function () {
        log('bindUrlList.click:' + $(this).attr('id'));

        var id = $(this).attr('id');
        if (id == _curItem)
            return;
        _curItem = id;
        $('#RowRes .pane_control').removeClass('touched');
        $(urlListItemSelector).removeClass('active');
        $(this).addClass('active');
        _curReq = { id: $(this)[0].id, req: $(this).children('SPAN').html() };
        editor.setValue($(this).attr('datares'));
        editor.gotoLine(0);
         $("#btnSave").addClass('disabled');
    });
}

function setUp() {

    $('#searchBar').on('keyup', function () {

        $("#ListReq").html('');
        var searchText = $(this).val();


        createUrlList(Lazy(items).filter(function (url) {
            return url.req.includes(searchText);
        }).toArray());
    });



    $('#BFilter').on('click', function () {
        send('FILTER_UP', { domain: $('#DomainFilter INPUT').val(), request: $('#XhrFilter INPUT').val() });
    });





    $('#IUp').on('click', function () {
        send('CACHE_EXPORT');
    });
    $('#IDown').on('click', function () {
        //* get file
        $('#FakeInp').detach();
        var eInp = $('<input id="FakeInp" type="file" style="display:none;">');
        $('BODY').append(eInp).promise().done(function () {
            $('#FakeInp').on('change', function () {
                if (!$('#FakeInp')[0].files.length)
                    return;
                var file = $('#FakeInp')[0].files[0];
                if (!file.name.match(/idb_/)) {
                    alert('INVALID_FILE');
                    return;
                }
                log('TO_READ:');
                var reader = new FileReader();
                reader.onload = function () {
                    log(reader.result.length);
                    var data = JSON.parse(reader.result);
                    send('CACHE_IMPORT', data);
                };
                reader.readAsText(file);
            });
            $('#FakeInp').click();
        });
    });
}

$('#btnSave').on('click', function ($event) {
    var jnew = editor.getValue();
    _curReq.res = jnew;
    send('SAVE_CACHE_EDIT', { 'req': _curReq.req, 'res': jnew });
    $(this).addClass('disabled');
    $event.stopPropagation();
    
});

function saveFile(name, type, data) {
    lo('saveFile:' + name);

    var a = $("<a style='display:none;'/>");
    //    var a = $("<a style='position:fixed;top:100px;left:100px;'>DOWN</a>");
    a.attr("download", name);
    var url = window.URL.createObjectURL(new Blob([data], { type: type }));
    a.attr("href", url);
    $("body").append(a).promise().done(function () {
        lo('LINK_READY:');
        a[0].click();
        a.remove();
    });
}

function react(msg) {

    switch (msg.subject) {
        case 'TO_LIST': console.log('TO_LIST:' + JSON.stringify(msg.body) + "::" + items.length);
            items.unshift(msg.body);
            console.dir(items);
            createUrlList(items);
            break;
        case 'CACHE_EXPORT':
            console.dir(msg);
            var dt = new Date();
            var mm = dt.getMonth() + 1;
            var dd = dt.getDate();
            saveFile('idb_' + [dt.getFullYear(), !mm[1] && '0', mm, !dd[1] && '0', dd].join('') + '.back', "data:attachment/text", JSON.stringify(msg.body));
            //        fdown('idb_' + [dt.getFullYear(), !mm[1] && '0', mm, !dd[1] && '0', dd].join('') + '.back', );
            break;
        case 'SAVE_CACHE_EDIT':
            log('react.SAVE_CACHE_EDIT:' + _curReq);
            $('#' + _curReq.id).attr('datares', _curReq.res);
            break;
        case 'CACHE_GET':
            items = msg.body;

            editor = ace.edit("jsonEditor");
            editor.getSession().setMode("ace/mode/json");
            editor.getSession().doc.on('change', function (e) {
                var jnew = editor.getValue();
                log('blank.CACHE_GET:');
                _curReq.res = jnew;
                $("#btnSave").removeClass('disabled');
                console.dir(_curReq);
                send('SAVE_CACHE_EDIT', { 'req': _curReq.req, 'res': jnew });
            });
            createUrlList(msg.body);
            setUp();
            log('react.' + msg.subject);
            console.dir(msg);
            break;
        case 'CACHE_IMPORT':
            send('CACHE_GET', null);
            break;
    }
}
