/**
 * Created by X on 11/10/2016.
 */
DBG = 1;
log('- POPUP:');

var _mode, _filter;

send('GET_MODE', null, function(res) { log('GET_MODE:OUT:'+JSON.stringify(res));
    _mode = res.mode;
    _filter = res.filter;
    setUp();
});

//setUp();

function setUp() {  log('popup.setUp:');
//??
            //select($('#MdRec')[0]);
            //send('MODE_REC', {id: 'MdRec'});
    $('#Close').on('click', function() {
        window.close();
    });
    $('#MdRec').on('click', function() {
        select(this);
        send('MODE_REC', {id: 'MdRec'});
    });
    $('#MdPlay').on('click', function() {
        select(this);
        send('MODE_PLAY', {id: 'MdPlay'});
    });
    $('#MdEdit').on('click', function() {
        select(this);
        var tabURL = '/app/index.html';
        if (DBG == 2)
            tabURL = '/xhrcache/views/plain/blank.html';
        chrome.runtime.sendMessage({
            subject: 'NEW_TAB',
            body: 'chrome-extension://'+chrome.runtime.id+tabURL,
            tab: 'BLANK'
        });
//        send('MODE_EDIT', {id: 'MdEdit'});
    });
    $('#MdOff').on('click', function() {
        select(this);
        send('MODE_OFF', {id: 'MdOff'});
    });
    if (_mode && _mode.id) {
        $('#' + _mode.id).addClass('sel');
    }
    if (_filter) {
        $('#ReqURL').val(_filter.request);
        $('#DomURL').val(_filter.domain);
    }
}

function select(obj) {

    $('.mode_ctrl').removeClass('sel');
    $(obj).addClass('sel');
    window.close();
}
