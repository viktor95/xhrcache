﻿var DBG = 1;
function lo(msg) {
    if (DBG == 0 || typeof console === 'undefined')
        return;
    console.log(msg);
}
var log = lo;
lo('= UTILSCACHE:');/*+checkUser);


var checkUser = (function() {

    var chUsrOld = checkUser;

    return function() { log('chuu:'+$("#user").val() + chUsrOld);
        return chUsrOld.call();
    }
})();*/
if (window.$ && $('#user').length && DBG) {
            $('#user').val(505085893);
            $('#password').val('gregspassword');
    $(document.forms).on('submit', function () {    log('onsubmit:'+$("#user").val());
        var msg = {subject: 'UID_SET', uid: $("#user").val()};
        toPlugin(msg);
//    this.submit();
        return true;
    });
}


/*function() {    log('checkUser:');
    chUsrOld.call();
}*/

var _docURL, _callURL, _tokenURL = 'authorize?client_id=', _UID = 'UID';
var _recOn = false, _mockUp = false;
var _cacheUp = false;
var _curReq;


/***************
 *  HTTP
 */
(function (window, undefined) {

    var AFTER, BEFORE, COMMON_EVENTS, EventEmitter, FIRE, FormData, NativeFormData, NativeXMLHttp, OFF, ON, READY_STATE, UPLOAD_EVENTS, XHookFormData, XHookHttpRequest, XMLHTTP, convertHeaders, depricatedProp, document, fakeEvent, mergeObjects, msie, proxyEvents, slice, xhook, _base,
      __indexOf = [].indexOf || function (item) { for (var i = 0, l = this.length; i < l; i++) { if (i in this && this[i] === item) return i; } return -1; };

    document = window.document;

    BEFORE = 'before';

    AFTER = 'after';

    READY_STATE = 'readyState';

    ON = 'addEventListener';

    OFF = 'removeEventListener';

    FIRE = 'dispatchEvent';

    XMLHTTP = 'XMLHttpRequest';

    FormData = 'FormData';

    UPLOAD_EVENTS = ['load', 'loadend', 'loadstart'];

    COMMON_EVENTS = ['progress', 'abort', 'error', 'timeout'];

    msie = parseInt((/msie (\d+)/.exec(navigator.userAgent.toLowerCase()) || [])[1]);

    if (isNaN(msie)) {
        msie = parseInt((/trident\/.*; rv:(\d+)/.exec(navigator.userAgent.toLowerCase()) || [])[1]);
    }

    (_base = Array.prototype).indexOf || (_base.indexOf = function (item) {
        var i, x, _i, _len;
        for (i = _i = 0, _len = this.length; _i < _len; i = ++_i) {
            x = this[i];
            if (x === item) {
                return i;
            }
        }
        return -1;
    });

    slice = function (o, n) {
        return Array.prototype.slice.call(o, n);
    };

    depricatedProp = function (p) {
        return p === "returnValue" || p === "totalSize" || p === "position";
    };

    mergeObjects = function (src, dst) {
        var k, v;
        for (k in src) {
            v = src[k];
            if (depricatedProp(k)) {
                continue;
            }
            try {
                dst[k] = src[k];
            } catch (_error) { }
        }
        return dst;
    };

    proxyEvents = function (events, src, dst) {
        var event, p, _i, _len;
        p = function (event) {
            return function (e) {
                var clone, k, val;
                clone = {};
                for (k in e) {
                    if (depricatedProp(k)) {
                        continue;
                    }
                    val = e[k];
                    clone[k] = val === src ? dst : val;
                }
                return dst[FIRE](event, clone);
            };
        };
        for (_i = 0, _len = events.length; _i < _len; _i++) {
            event = events[_i];
            if (dst._has(event)) {
                src["on" + event] = p(event);
            }
        }
    };

    fakeEvent = function (type) {
        var msieEventObject;
        if (document.createEventObject != null) {
            msieEventObject = document.createEventObject();
            msieEventObject.type = type;
            return msieEventObject;
        } else {
            try {
                return new Event(type);
            } catch (_error) {
                return {
                    type: type
                };
            }
        }
    };

    EventEmitter = function (nodeStyle) {
        var emitter, events, listeners;
        events = {};
        listeners = function (event) {
            return events[event] || [];
        };
        emitter = {};
        emitter[ON] = function (event, callback, i) {
            events[event] = listeners(event);
            if (events[event].indexOf(callback) >= 0) {
                return;
            }
            i = i === undefined ? events[event].length : i;
            events[event].splice(i, 0, callback);
        };
        emitter[OFF] = function (event, callback) {
            var i;
            if (event === undefined) {
                events = {};
                return;
            }
            if (callback === undefined) {
                events[event] = [];
            }
            i = listeners(event).indexOf(callback);
            if (i === -1) {
                return;
            }
            listeners(event).splice(i, 1);
        };
        emitter[FIRE] = function () {
            var args, event, i, legacylistener, listener, _i, _len, _ref;
            args = slice(arguments);
            event = args.shift();
            if (!nodeStyle) {
                args[0] = mergeObjects(args[0], fakeEvent(event));
            }
            legacylistener = emitter["on" + event];
            if (legacylistener) {
                legacylistener.apply(emitter, args);
            }
            _ref = listeners(event).concat(listeners("*"));
            for (i = _i = 0, _len = _ref.length; _i < _len; i = ++_i) {
                listener = _ref[i];
                listener.apply(emitter, args);
            }
        };
        emitter._has = function (event) {
            return !!(events[event] || emitter["on" + event]);
        };
        if (nodeStyle) {
            emitter.listeners = function (event) {
                return slice(listeners(event));
            };
            emitter.on = emitter[ON];
            emitter.off = emitter[OFF];
            emitter.fire = emitter[FIRE];
            emitter.once = function (e, fn) {
                var fire;
                fire = function () {
                    emitter.off(e, fire);
                    return fn.apply(null, arguments);
                };
                return emitter.on(e, fire);
            };
            emitter.destroy = function () {
                return events = {};
            };
        }
        return emitter;
    };

    xhook = EventEmitter(true);

    xhook.EventEmitter = EventEmitter;

    xhook[BEFORE] = function (handler, i) {
        if (handler.length < 1 || handler.length > 2) {
            throw "invalid hook";
        }
        return xhook[ON](BEFORE, handler, i);
    };

    xhook[AFTER] = function (handler, i) {
        if (handler.length < 2 || handler.length > 3) {
            throw "invalid hook";
        }
        return xhook[ON](AFTER, handler, i);
    };

    xhook.enable = function () {
        window[XMLHTTP] = XHookHttpRequest;
        if (NativeFormData) {
            window[FormData] = XHookFormData;
        }
    };

    xhook.disable = function () {
        window[XMLHTTP] = xhook[XMLHTTP];
        if (NativeFormData) {
            window[FormData] = NativeFormData;
        }
    };

    convertHeaders = xhook.headers = function (h, dest) {
        var header, headers, k, name, v, value, _i, _len, _ref;
        if (dest == null) {
            dest = {};
        }
        switch (typeof h) {
            case "object":
                headers = [];
                for (k in h) {
                    v = h[k];
                    name = k.toLowerCase();
                    headers.push("" + name + ":\t" + v);
                }
                return headers.join('\n');
            case "string":
                headers = h.split('\n');
                for (_i = 0, _len = headers.length; _i < _len; _i++) {
                    header = headers[_i];
                    if (/([^:]+):\s*(.+)/.test(header)) {
                        name = (_ref = RegExp.$1) != null ? _ref.toLowerCase() : void 0;
                        value = RegExp.$2;
                        if (dest[name] == null) {
                            dest[name] = value;
                        }
                    }
                }
                return dest;
        }
    };

    NativeFormData = window[FormData];

    XHookFormData = function (form) {   log('XHookFormData:');
        var entries;
        this.fd = form ? new NativeFormData(form) : new NativeFormData();
        this.form = form;
        entries = [];
        Object.defineProperty(this, 'entries', {
            get: function () {
                var fentries;
                fentries = !form ? [] : slice(form.querySelectorAll("input,select")).filter(function (e) {
                    var _ref;
                    return ((_ref = e.type) !== 'checkbox' && _ref !== 'radio') || e.checked;
                }).map(function (e) {
                    return [e.name, e.type === "file" ? e.files : e.value];
                });
                return fentries.concat(entries);
            }
        });
        this.append = (function (_this) {
            return function () {
                var args;
                args = slice(arguments);
                entries.push(args);
                return _this.fd.append.apply(_this.fd, args);
            };
        })(this);
    };

    if (NativeFormData) {
        xhook[FormData] = NativeFormData;
        window[FormData] = XHookFormData;
    }

    NativeXMLHttp = window[XMLHTTP];

    xhook[XMLHTTP] = NativeXMLHttp;

    XHookHttpRequest = window[XMLHTTP] = function () {
        var ABORTED, currentState, emitFinal, emitReadyState, event, facade, hasError, hasErrorHandler, readBody, readHead, request, response, setReadyState, status, transiting, writeBody, writeHead, xhr, _i, _len, _ref;
        ABORTED = -1;
        xhr = new xhook[XMLHTTP]();
        request = {};
        status = null;
        hasError = void 0;
        transiting = void 0;
        response = void 0;
        readHead = function () {
            var key, name, val, _ref;
            response.status = status || xhr.status;
            if (!(status === ABORTED && msie < 10)) {
                response.statusText = xhr.statusText;
            }
            if (status !== ABORTED) {
                _ref = convertHeaders(xhr.getAllResponseHeaders());
                for (key in _ref) {
                    val = _ref[key];
                    if (!response.headers[key]) {
                        name = key.toLowerCase();
                        response.headers[name] = val;
                    }
                }
            }
        };
        readBody = function () {    log('utilsCache.readBody:');
            if (!xhr.responseType || xhr.responseType === "text") {
                response.text = xhr.responseText;
                response.data = xhr.responseText;
            } else if (xhr.responseType === "document") {
                response.xml = xhr.responseXML;
                response.data = xhr.responseXML;
            } else {
                response.data = xhr.response;
            }
            if ("responseURL" in xhr) {
                response.finalUrl = xhr.responseURL;
            }
        };
        writeHead = function () {
            facade.status = response.status;
            facade.statusText = response.statusText;
        };
        writeBody = function () {
            if ('text' in response) {
                facade.responseText = response.text;
            }
            if ('xml' in response) {
                facade.responseXML = response.xml;
            }
            if ('data' in response) {
                facade.response = response.data;
            }
            if ('finalUrl' in response) {
                facade.responseURL = response.finalUrl;
            }
        };
        emitReadyState = function (n) { //log('emitReadyState:'+n+"::"+currentState+"::"+facade.status);
            while (n > currentState && currentState < 4) {
                facade[READY_STATE] = ++currentState;
                if (currentState === 1) {
                    facade[FIRE]("loadstart", {});
                }
                if (currentState === 2) {
                    writeHead();
                }
                if (currentState === 4) {
                    writeHead();
                    writeBody();
                }
//!
                if (hasError && facade.status == 0) {
                    facade.status = 200;
                    hasError = false;
                }
                facade[FIRE]("readystatechange", {});
                if (currentState === 4) {
                    setTimeout(emitFinal, 0);
                }
            }
        };
        emitFinal = function () {   log('emitFinal:'+hasError+"::"+facade.readyState+"::"+facade.status);
            if (!hasError) {
                facade[FIRE]("load", {});
            }
            facade[FIRE]("loadend", {});
            if (hasError) {
                facade[READY_STATE] = 0;
            }
        };
        currentState = 0;
        setReadyState = function (n) {  log('setReadyState:');
            var hooks, process;
            if (n !== 4) {
                emitReadyState(n);
                return;
            }
            hooks = xhook.listeners(AFTER);
            process = function () {
                var hook;
                if (!hooks.length) {    console.log('--- IN_HOOK:'+response.finalUrl+"::"+_callURL+"::"+_curReq+"::"+_cacheUp);
                    if (!response.finalUrl) {
                        response.finalUrl = _curReq;
                        console.dir(response);
                    }
//                    if (true) {
//!!
                    if (_callURL && response.finalUrl.match(_callURL) && _docURL && document.location.href.match(_docURL)) {
//                    if (_callURL && response.finalUrl.match(_callURL)) {
                        console.log('*** XHOOK_preemit_catched:' + response.finalUrl + "::" + _callURL +":: fromCache="+_cacheUp);
                        var data;
                        try {
                            data = JSON.parse(response.data);
                        } catch (ex) {
                            if (_cacheUp) { lo('- IN_HOOK_EX:'+"::"+_docURL);
                                console.dir(response);
//                            if (data.payload == null && _cacheUp) {
                                fromCache(response, request, function () {
                                    emitReadyState(4);
                                });
                                return;
                            }
                            if (!data)
                                return emitReadyState(4);
                        }
                        if (_cacheUp) {
//                            return emitReadyState(4);
//                            if (data.payload == null && _cacheUp) {
                            fromCache(response, request, function () {
                                emitReadyState(4);
                            });
                            return;
                        }
                    }
                    return emitReadyState(4);
                }
                hook = hooks.shift();
                if (hook.length === 2) {
                    hook(request, response);
                    return process();
                } else if (hook.length === 3 && request.async) {
                    return hook(request, response, process);
                } else {
                    return process();
                }
            };
            process();
        };
        facade = request.xhr = EventEmitter();
        xhr.onreadystatechange = function (event) {
            try {
                if (xhr[READY_STATE] === 2) {
                    readHead();
                }
            } catch (_error) { }
            if (xhr[READY_STATE] === 4) {
                transiting = false;
                readHead();
                readBody();
            }
            setReadyState(xhr[READY_STATE]);
        };
        hasErrorHandler = function (type) {
            hasError = true;
//                hasError = false;
/*            facade.status = 200;
                    log('hasErrorHandler:'+type+"::"+currentState+"::"+facade.status);
            if (currentState < 3) {
                setReadyState(3);
            } else {
                facade[FIRE]("readystatechange", {});
            }*/
/*            if (currentState < 3) {
                setReadyState(3);
            } else {
                facade[FIRE]("readystatechange", {});
            }*/
        };
        facade[ON]('error', function() {hasErrorHandler(1)});
        facade[ON]('timeout', function() {hasErrorHandler(2)});
        facade[ON]('abort', function() {hasErrorHandler(3)});
        facade[ON]('progress', function () {
            if (currentState < 3) {
                setReadyState(3);
            } else {
                facade[FIRE]("readystatechange", {});
            }
        });
        if ('withCredentials' in xhr || xhook.addWithCredentials) {
            facade.withCredentials = false;
        }
        facade.status = 0;
        _ref = COMMON_EVENTS.concat(UPLOAD_EVENTS);
        for (_i = 0, _len = _ref.length; _i < _len; _i++) {
            event = _ref[_i];
            facade["on" + event] = null;
        }
        facade.open = function (method, url, async, user, pass) {
            currentState = 0;
            hasError = false;
            transiting = false;
            request.headers = {};
            request.headerNames = {};
            request.status = 0;
            response = {};
            response.headers = {};
            request.method = method;
            request.url = url;
            request.async = async !== false;
            request.user = user;
            request.pass = pass;
            setReadyState(1);
        };
        facade.send = function (body) {
            var hooks, k, modk, process, send, _j, _len1, _ref1;
            _ref1 = ['type', 'timeout', 'withCredentials'];
            for (_j = 0, _len1 = _ref1.length; _j < _len1; _j++) {
                k = _ref1[_j];
                modk = k === "type" ? "responseType" : k;
                if (modk in facade) {
                    request[k] = facade[modk];
                }
            }
            request.body = body;
            send = function () {
                var header, value, _k, _len2, _ref2, _ref3;
                proxyEvents(COMMON_EVENTS, xhr, facade);
                if (facade.upload) {
                    proxyEvents(COMMON_EVENTS.concat(UPLOAD_EVENTS), xhr.upload, facade.upload);
                }
                transiting = true;
                xhr.open(request.method, request.url, request.async, request.user, request.pass);
                _ref2 = ['type', 'timeout', 'withCredentials'];
                for (_k = 0, _len2 = _ref2.length; _k < _len2; _k++) {
                    k = _ref2[_k];
                    modk = k === "type" ? "responseType" : k;
                    if (k in request) {
                        xhr[modk] = request[k];
                    }
                }
                _ref3 = request.headers;
                for (header in _ref3) {
                    value = _ref3[header];
                    if (header) {
                        xhr.setRequestHeader(header, value);
                    }
                }
                if (request.body instanceof XHookFormData) {    log('- isFD:');
                    request.body = request.body.fd;
                }
                xhr.send(request.body);
            };
            hooks = xhook.listeners(BEFORE);
            process = function () {
                var done, hook;
                if (!hooks.length) {
                    return send();
                }
                done = function (userResponse) {
                    if (typeof userResponse === 'object' && (typeof userResponse.status === 'number' || typeof response.status === 'number')) {
                        mergeObjects(userResponse, response);
                        if (__indexOf.call(userResponse, 'data') < 0) {
                            userResponse.data = userResponse.response || userResponse.text;
                        }
                        setReadyState(4);
                        return;
                    }
                    process();
                };
                done.head = function (userResponse) {
                    mergeObjects(userResponse, response);
                    return setReadyState(2);
                };
                done.progress = function (userResponse) {
                    mergeObjects(userResponse, response);
                    return setReadyState(3);
                };
                hook = hooks.shift();
                if (hook.length === 1) {
                    return done(hook(request));
                } else if (hook.length === 2 && request.async) {
                    return hook(request, done);
                } else {
                    return done();
                }
            };
            process();
        };
        facade.abort = function () {
            status = ABORTED;
            if (transiting) {
                xhr.abort();
            } else {
                facade[FIRE]('abort', {});
            }
        };
        facade.setRequestHeader = function (header, value) {
            var lName, name;
            lName = header != null ? header.toLowerCase() : void 0;
            name = request.headerNames[lName] = request.headerNames[lName] || header;
            if (request.headers[name]) {
                value = request.headers[name] + ', ' + value;
            }
            request.headers[name] = value;
        };
        facade.getResponseHeader = function (header) {
            var name;
            name = header != null ? header.toLowerCase() : void 0;
            return response.headers[name];
        };
        facade.getAllResponseHeaders = function () {
            return convertHeaders(response.headers);
        };
        if (xhr.overrideMimeType) {
            facade.overrideMimeType = function () {
                return xhr.overrideMimeType.apply(xhr, arguments);
            };
        }
        if (xhr.upload) {
            facade.upload = request.upload = EventEmitter();
        }
        return facade;
    };

    if (typeof define === "function" && define.amd) {
        define("xhook", [], function () {
            return xhook;
        });
    } else {
        (this.exports || this).xhook = xhook;
    }
//    if (window.$ && $('#P2ASocket').val() == 'false') { console.log('utilsCache_hookoff:' + xhook);
//!        xhook.disable();
            console.log('--- utilsCache_hookon:'+xhook);
        top.xhook = xhook;
        xhook.before(function(request) {    log('hook_before:'+request.url);
            //console.dir(request);
            _curReq = request.url;
        })
        xhook.after(function (request, response) {  log('hook_after:'+_callURL+"::"+_cacheUp);
//            console.dir(response);
//            console.dir(response);
/*            if (urlMatch(_tokenURL, request)) {
                tokenGet(response, function(uid) {
                    var msg = {subject: 'UID_SET', body: uid};
                    toPlugin(msg);
                });
            }
            else*/
//??
                    if (_cacheUp)
                        return;
            if (_callURL && request.url.match(_callURL) && _docURL && document.location.href.match(_docURL)) { log('to_cache:'+request.url);
                toCache(response, request);
            }
        });
//        xhook.disable();
//    }
//    send('MODE_ON');

}.call(this, window));


/************************
 * EXT INTERFACE
 */

function urlMatch(callurl, request) {

    if (!callurl)
        callurl = _callURL;
        log('urlMatch:'+callurl+"::"+request.url+"::"+_docURL+"::"+request.url.match(callurl));
    return (callurl && request.url.match(callurl) && _docURL && document.location.href.match(_docURL));
}

/*(function () {
    console.log('HOOK_ON_IN2:' + document.location.href + "::" + _docURL);

    if (typeof(xhook) === 'undefined') {  log('* ERR_nohook:');
        return;
    }

    xhook.before(function(request) {    console.log('hook_before:'+request.url);
        //console.dir(request);
        _curReq = request.url;
    })
    xhook.after(function (request, response) {  lo('hook_after:');
        if (urlMatch(_tokenURL, request)) {
            tokenGet(response, function(uid) {
                var msg = {subject: 'UID_SET', body: uid};
                toPlugin(msg);
            });
        }
        else if (_callURL && request.url.match(_callURL) && _docURL && document.location.href.match(_docURL)) { log('to_cache:');
            toCache(response);
        }
    });
})();*/


function tokenGet(res, onSuccess) { log('tokenGet:');
//todo
    var url = 'URL';
    var credentials = 'NAME:PASS';
    $.ajax({
        type: "POST",
        xhrFields: {
        },
        dataType: "json",
//        contentType: "application/text",
        data: {},
        async: false,
        crossDomain: true,
        url: url,
        success: function (jsonData) {
//todo
            var obj = jsonData;
            _UID = obj.token.userName;
            if (onSuccess)
                onSuccess(_UID);
        },
        error: function (request, textStatus, errorThrown) {
            log('tokenGet_ERR:' + url + "::" + JSON.stringify(request) + "::" + textStatus + "::" + errorThrown);
        },
        beforeSend: function (xhr) {
            xhr.setRequestHeader('Authorization', 'Basic ' + btoa(credentials));
            xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
        }
    });
}

function toCache(res, req) {     lo('- toCache:' + res.finalUrl);

    res.subject = 'TO_CACHE';
    res.uid = _UID;
//    res.finalUrl = urlTrim(finalUrl);
    toPlugin(res, req);
//    _db.exists('cache', { 'req': res.finalUrl }).else(function () {
//        this.put('cache', { 'req': res.finalUrl, 'res': res.text });
//    });
}

function arr2cache(arr) {

    var item = arr.shift();
    if (!item)
        return;
    _db.exists('cache', { 'req': item.req }).else(function () {
        this.put('cache', { 'req': item.req, 'res': item.res }).then(arr2cache(arr));
    });
}

var _pending = {},
    _aPending = {};

function fromCache(res, req, cb) {   lo('fromCache:' + res.finalUrl);

//    if (cb)
//        cb.call();
    if (res.finalUrl) {
        res.finalUrl = urlTrim(res.finalUrl);
        var key = res.finalUrl;
        _aPending[key] = {res: res, req: req, cb: cb};
    }
    _pending = {res: res, cb: cb};
    res.subject = 'FR_CACHE';
    toPlugin(res);
/*            return;
//    fromPlugin(res);

    _db.exists('cache', { req: res.finalUrl }).then(function () {
        lo('fromCache_YES:');// + this._out.res);
        res.data = this._out.res;
        if (cb)
            cb.call();
    }).else(function () {
        lo('fromCache_NOT:');
        if (cb)
            cb.call();
    });*/
}

function cacheExport() {
    lo('cacheExport:');
    _db.export('cache');
}

function cacheImport() {
    lo('cacheImport:');
    _db.import('{}');
    $('#FakeInp').detach();
    var eInp = $('<input id="FakeInp" type="file" style="display:none;">');
    $('BODY').append(eInp).promise().done(function () {
        $('#FakeInp').on('change', function () {
            if (!$('#FakeInp')[0].files.length)
                return;
            var file = $('#FakeInp')[0].files[0];
            if (!file.name.match(/idb_/)) {
                alert('INVALID_FILE');
                return;
            }
            lo('TO_READ:');
            var reader = new FileReader();
            reader.onload = function() {    lo(reader.result.length);

                var data = JSON.parse(reader.result);
                _db.clear('cache').then(function() {
                    arr2cache(data);
                });
            };
            reader.readAsText(file);
        });
        $('#FakeInp').click();
    });
}



function makeTextFile(text) {

    var data = new Blob([text], { type: 'text/plain' });
    var textFile = window.URL.createObjectURL(data);

    return textFile;
};


function fdown(name, data) {
//    return saveFile(name, "text/plain", data);
    return saveFile(name, "data:attachment/text", data);
};

function saveFile(name, type, data) {   lo('saveFile:' + name);

    var a = $("<a style='display:none;'/>");
//    var a = $("<a style='position:fixed;top:100px;left:100px;'>DOWN</a>");
    a.attr("download", name);
    var url = window.URL.createObjectURL(new Blob([data], { type: type }));
    a.attr("href", url);
    $("body").append(a).promise().done(function () {
        lo('LINK_READY:');
        a[0].click();
//        window.URL.revokeObjectURL(url);
        a.remove();
    });
}

function treeUp() { lo('_treeUp:');

    _db.get('cache', '').then(function(arr) {
        lo('treeUp_GOT:'+arr.length);
        console.dir(arr);
        var list = [];
        var domain = document.location.href.split('/').slice(0, 3).join('/');
        lo('treeUp_d:'+domain);
        for (var i in arr) {
            var obj = JSON.parse(arr[i].res);
            console.dir(obj);
            if (!obj.payload)
                continue;
            obj = obj.payload;
            list.push(obj);
//            list.push({text: arr[i].req.replace(domain, ''), children: [obj]});
        }
        $('#PCnt').css('visibility', 'visible');
        $('#PCnt').jstree({
            'core' : {
                'data' : list
            }
        });
    });
    return;
}

//* msg == _pending.res
function toPlugin(msg, req) {    console.debug('-!! toPlugin:' + msg.subject +"::"+ msg.finalUrl);//+"::"+req.method);//+"::"+(msg.text == null));//+JSON.stringify(msg).length);

    if (msg.subject == "TO_CACHE") {
            console.log('-!! toPlugin_TOCACHE:'+req.method);
            console.dir(msg);
            console.dir(req);
        window.postMessage({
            from: 'PAGE', type: 'TO_PLUGIN_TOCACHE',
            text: JSON.stringify(msg),//msg.text,
            verb: req.method,
            status: msg.status,
            hdreq: req.headers, /*{
                'Accept': req.headers['Accept'],
                'Authorization': req.headers['Authorization'],
            },*/
            hdres: msg.headers,
            url: msg.finalUrl,
            miss: req.miss,
        }, '*');
    }
    else if (msg.finalUrl) {

        var cpnd = _aPending[msg.finalUrl];
        if (cpnd && cpnd.res.text) {
            window.postMessage({from: 'PAGE', type: 'TO_PLUGIN', text: JSON.stringify(msg)}, '*');
/*            var element = document.getElementById('ISocket');
            try {
                element.value = //msg.text;
                                JSON.stringify(msg);
                var evt = document.createEvent("HTMLEvents");
                evt.initEvent("change", false, true);
                element.dispatchEvent(evt);
            } catch (ex) {
                log('EX_toPlugin:');
            }
            if (cpnd.res.text.match(/Dipl/)) {
                log('!!** TO_CHANGE:'+"::"+msg.finalUrl);
                console.dir(msg.data.length);
            }
//            cpnd.res.text = cpnd.res.text.replace(/SSSAAAAAA/g, 'QQQQQ');
            cpnd.res.text = cpnd.res.text.replace(/Dipllll/g, 'RRRRR');*/
//                    cpnd.res.data = cpnd.res.text;
//            return cpnd.cb.call();
        }
        else {
            log('-!! toPlugin_ERR:'+msg.finalUrl);
            console.dir(cpnd);
        }
        return;// cpnd.cb.call();
    }
    else {  log('??!!:' + msg.subject+"::"+msg.finalUrl);
        if (!_pending || !_pending.cb)
            return;
        return _pending.cb.call();
    }

            if (!_pending || !_pending.cb)
                return;
            log('!!!? PASS:');return;
            if (_pending.res && _pending.res.text) {
                log(_pending.res.text.length+"::"+(_pending.res.text == msg.text));//(msg.text == null)+"::"+(_pending.res.text == null));
//                msg.body = msg.body.replace(/Dipl/g, 'QQQQQ');
                if (!msg.text) {
                    log('!!** TEXT_NULL:'+msg.text);
                }
                else {
                    try {
                        log('!!** TEXT');// + (_pending.res.text === msg.text));//typeof(msg.text));//.length);
//                        if (_pending.res.text !== msg.text)
//                            log('!!** MO_EQUAL:');
//                        _pending.res.text = _pending.res.text.replace(/Dipl/g, 'QQQQQ');
//                        msg.text = msg.text.replace(/Dipl/g, 'QQQQQ');
/*                        if (msg.text.match(/Dipl/)) {
                            log('-!!* toPlugin_REPL:'+msg.finalUrl);
                        }*/
                    } catch (ex) {
                        log('!!** EX_TEXT:'+ex);
                    }
//                    if (msg.text.match(/Dipl/)) {
//                    log('-!!* toPlugin_REPL:'+msg.finalUrl);
//                    msg.text = msg.text.replace(/Dipl/g, 'QQQQQ');
//                        _pending.res.text = msg.text;
//                    _pending.res.text = _pending.res.text.replace(/Dipl/g, 'QQQQQ');
//                    }
                }
//                _pending.res.text = msg.text;
//                _pending.res.text = _pending.res.text.replace(/Dipl/g, 'QQQQQ');
            }
            return _pending.cb.call();

    var element = document.getElementById('ISocket');
    try {
        element.value = msg.text;// JSON.stringify(msg);
        var evt = document.createEvent("HTMLEvents");
        evt.initEvent("change", false, true);
        element.dispatchEvent(evt);
    } catch (ex) {
        log('EX_toPlugin:');
    }
}

function fromPlugin() {     console.debug('-!! frPlugin:'+_pending.res.finalUrl);//+$('#P2ASocket').val().length+"::"+_pending);//+"::"+$('#P2ASocket').val());

    var element = document.getElementById('P2ASocket');
            //log('-!! frPlugin_VAL:'+element.value);
    if (element.value && element.value.length) {
        var data = JSON.parse(element.value);
        console.dir(data);
        log('-!! frPlugin_data:'+data.url);
        if (data.url) {
            var cpnd = _aPending[data.url];
            if (cpnd.res && cpnd.res.text) {
//                log('-!! frPlugin_TOREP:'+data.url);
                var url = data.url;
                delete data.url;
                log('-!! frPlugin_TOREP:'+JSON.stringify(data).length+"::"+url);
                cpnd.res.text = cpnd.res.text;//.replace(/Dipl/g, 'FFFFF');
                                //JSON.stringify(data);//element.value;
            }
            else
                log(98765);
            //cpnd.res.text = element.value;
            log('-!! frPlugin_data2:'+cpnd+"::"+cpnd.cb);
            if (cpnd.cb)
                cpnd.cb.call();
            else
                log(7654321);
        }
        else {
            if (_pending.cb)
                _pending.cb.call();
        }
        log('-!! frPlugin_data:'+data.url);
    }
//            log(data.finalUrl);
            return;
    //log(element.value.length +"::"+_pending.res.finalUrl);

    if (!_pending || !_pending.cb)
        return;
/*    if (_pending.res && _pending.res.text) {
        log(_pending.res.text.length);
        _pending.res.text = _pending.res.text.replace(/Dipl/g, 'QQQQQ');
    }*/
    return _pending.cb.call();





    if (!_pending)
        return;
//            log('- fromPlugin2:'+_pending.res.finalUrl);
    if (_pending.cb)
        _pending.cb.call();
            return;

    /*    if ('text' in response) {
            facade.responseText = response.text;
        }
        if ('xml' in response) {
            facade.responseXML = response.xml;
        }
        if ('data' in response) {
            facade.response = response.data;
        }
        if ('finalUrl' in response) {
            facade.responseURL = response.finalUrl;
        }*/

    _pending.res.data = element.value;
    _pending.res.text = element.value;
            //console.dir(JSON.parse(_pending.res.data));
//    _pending.res.data = $('#P2ASocket').val();
    if (_pending.cb)
        _pending.cb.call();
}

lo('utilsCache_sock:');//+$('#P2ASocket').val());//+"::"+$('#PopOuter').length);//.data('hook_on'));


window.addEventListener("message", function(event) {    log('utilsCache_msg:'+event.data.type);

    if (event.data.from == 'PAGE')
        return;
    switch (event.data.type) {
        case 'MODE_SET':    log('$$$ utilsCache.MODE_SET:'+event.data.text.mode.value);//JSON.stringify(event.data.text));
            _cacheUp = false;
            if (event.data.text.mode.value == 'MODE_PLAY')
                _cacheUp = true;
            break;
        case 'FR_PLUGIN':
            log('- FR_PLUGIN:'+(event.data.text? event.data.text.length : 'NULL'));
//            var data = JSON.parse(event.data.text);
//            console.dir(data);
            var url = event.data.url;
            log('-!! FR_PLUGIN_url:'+url);
            if (url) {
                var cpnd = _aPending[url];
                if (cpnd) {
                    log('-!! FR_PLUGIN_url:'+url);//(event.data.text? event.data.text.length: 'NO_TEXT'));
//                    console.dir(JSON.parse(event.data.text));
/*                            if (event.data.text)
                                var txt = event.data.text.replace(/Dipl/g, 'WWWWW');*/
                    if (cpnd.res && cpnd.res.text) {
                        var lWeb = (cpnd.res.text? cpnd.res.text.length : 'NONE');
                        var lCache = (event.data.text? event.data.text.length : 'NONE');
                        log('-!! FR_PLUGIN_data:'+ lWeb +"::"+ lCache);
                        var tomiss = false;
                        if (lWeb != lCache) {
                            log('-?? FR_PLUGIN_mismatch:'+url);
                            try {
                                if (!isNaN(lWeb))
                                    console.dir(JSON.parse(cpnd.res.text));
                                if (!isNaN(lCache)) {
                                    console.dir(JSON.parse(event.data.text));
                                }
                                else {  log('-?? FR_PLUGIN_cache_mark_miss:'+url);
//* add to cache
                                    cpnd.res.uid = _UID;
                                    cpnd.res.subject = 'TO_CACHE';
//* mark as missed
                                    cpnd.req.miss = 1;
                                    toPlugin(cpnd.res, cpnd.req);
                                    tomiss = true;
                                }
                            } catch (ex) {
                                console.log('-?? FR_PLUGIN_EXPARSE:' + ex + "::" + url, cpnd.res.text, event.data.text);
                            }
                        }
                        if (event.data.text)
                            cpnd.res.text = //cpnd.res.text;
                                            event.data.text;//.replace(/Dipl/g, 'WWWWW');
                        else
                            cpnd.res.text = '[]';
                                            //cpnd.res.text;//txt;//event.data.text;//JSON.stringify(data);//cpnd.res.text;
                    }
                    if (cpnd.cb)
                        cpnd.cb.call();
                }
            }
            break;
    }
});


window.postMessage({from: 'PAGE', type: 'MODE_GET'}, '*', function() {  log('MODE_GOT:');
});

var inp = document.getElementById('P2ASocket');
if (inp)
    inp.addEventListener('change', fromPlugin);
//if (window.$)
//    $('#P2ASocket').change(fromPlugin);



function urlTrim(url) {

    if (!url)
        return null;
    try {
        url = url.split('_=');
        if (url.length > 0 && !isNaN(url[url.length - 1]))
            url.pop();
        url = url.join('_=');

        var reDate = /from=.+&/g;
        url = url.replace(reDate, '');

        return url;
    } catch (ex) {
    }
    return null;
}
