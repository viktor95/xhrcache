/**
 * Created by X on 11/8/2016.
 */
function lo(msg) {
    if (typeof console === 'undefined')
        return;
    console.log(msg);
}

var _dDB;
/******************
 * INDEXEDDB
 */

function DB(name) { lo('DB:' + name);//+"::"+Dexie);

    this._name = name;
    this._version = 0;
    if (typeof Dexie !== 'undefined')
        _dDB = new Dexie(name);
    this.up = function () {
        if (this._body)
            this._body.close();
        var req;
        if (!this._version)
            req = _iDB.open(this._name);
        else
            req = _iDB.open(this._name, ++this._version);
        var _this = this;
        req.onsuccess = function (e) {
            lo('onsuccess:');
            _this._body = e.target.result;
            _this._version = e.target.result.version;
            if (_this._then) {
                _this._then.call(_this);
                _this._then = null;
            }
        }
        req.onerror = function (e) {
            lo(222222);
        }
        req.onupgradeneeded = function (e) {
            lo("onupgradeneeded:" + e.target.result.version);// + "::" + _this._then + "::" + _this._version);
            _this._body = e.target.result;
            if (_this._onupgrade) {
                _this._onupgrade.call(_this);
                _this._onupgrade = null;
            }
        }
        return this;
    }
    this._tableUp = function(nm, col) { log('_tableUp:'+nm);
        var res = this._body.createObjectStore(nm, { keyPath: 'id', autoIncrement: true });
        for (var i in col) {
            res.createIndex(col[i].name, col[i].key, { unique: (col[i].unique || false)});
        }
    }
    this.storeUp = function(lst, cb) {  log('storeUp:');
//        var _this = this;
        var update = false;
        for (var i in lst) {
            if (lst[i].replace) {
                update = true;
                break;
            }
        }
        if (!update) {
            for (var i in lst) {
                if (!this._body.objectStoreNames.contains(lst[i].store)) {
                    update = true;
                    break;
                }
            }
        }
        if (!update) {  log('storeUp_NOUPD:');
            if (cb)
                cb.call();
            return;
        }
        this.up().onupgrade(function() {    log('storeUp_in:');
            for (var i in lst) {
                var nm = lst[i].store;
                if (this._body.objectStoreNames.contains(nm)) {
                    if (!lst[i].replace)
                        continue;
                    this._body.deleteObjectStore(nm);
                }
                this._tableUp(nm, lst[i].col);
            }
//            this._body.close();
            if (cb)
                cb.call();
        });
    }
    this.tableUp = function (nm) {
        lo('tableUp:' + nm + "::" + this._body + "::" + this._body.objectStoreNames.contains(nm));
        if (this._body.objectStoreNames.contains(nm))
            return null;
        var _this = this;
        //* run version change
        this.up().onupgrade(function () {
            //** create table
            var res = this._body.createObjectStore(nm, { keyPath: 'id', autoIncrement: true });
            res.createIndex('UID', 'uid', { unique: true });
            res.createIndex('Request', 'req', { unique: true });
            res.createIndex('Response', 'res', { unique: false });
            return res;
        });
        return this;
    }
    this.clear = function (table) {
        if (table) {
            //* clear table
            var transaction = this._body.transaction([table], "readwrite");
            var store = transaction.objectStore(table);
            var request = store.clear();
            var _this = this;
            request.onsuccess = function () {
                lo('clear_OK:' + table);
                if (_this._then)
                    _this._then.call(_this);
                _this._then = null;
            }
        }
        else {
            //* remove all tables
        }
        return this;
    }
    this.del = function(table, key, val) { log('del:' + key);
        var transaction = this._body.transaction([table], "readwrite");
        var store = transaction.objectStore(table);
        var tagIndex = store.index(key);
        var request = tagIndex.openKeyCursor(val);
//        var pdestroy = tagIndex.openKeyCursor(IDBKeyRange.only(val));
        var _this = this;
//        var request = store.delete(key).get(val);
//        var request = store.index(key).get(val);
        request.onsuccess = function(e) {
            var cursor = request.result;
                    log('del_ON:');
                    console.log(cursor);
            if (cursor.key == val) {
                store.delete(cursor.primaryKey);
            }
            _this.ondone.call(_this);
        }
        request.onerror = function(e) {   log('del_ERR:')
        }
        return this;
    }
    this.put = function (table, data, replace) {
        lo('put_to:' + table+"::"+data.req);
        console.dir(data);
        var transaction = this._body.transaction([table], "readwrite");
        var store = transaction.objectStore(table);

        var _this = this;
        if (replace) {
//            var request = store.get(data.id);
            var request = store.index('Request').get(data.req);
            request.onsuccess = function(e) {
                var dt = e.target.result;
                if (dt) {
                    dt.res = data.res;
                    dt.hdreq = data.hdReq;
                    dt.miss = data.miss;
                }
                else
                    dt = data;

                // Put this updated object back into the database.
                var requestUpdate = store.put(dt);
                requestUpdate.onerror = function(event) {
                    log('update_NO:');
                };
                requestUpdate.onsuccess = function(event) {
                    log('update_YES:');
                    if (_this._then)
                        _this._then.call(_this);
                    _this._then = null;
                    _this._ifyes = null;
                };
            };
            request.onerror = function(event) {
                log('db.put_replcae_ERR:');
            };
            return this;
        }
        var request = store.add(data);
        request.onsuccess = function (e) {  lo("put_DONE:"+data.req);
            _this.ondone();
        }
        request.onerror = function (e) {    lo("put_ERR:", e.target.error.name);
        }
        return this;
    }
    this.fetch = function(table, filter, cb) {  log('fetch:'+_dDB);//+this._body.isOpen());return;
        _dDB.open().then(function(db) {
            var clst = db.table(table);
            var keys = Object.keys(filter);
            for (var i in keys) {
                var val = filter[keys[i]];
                if (val == undefined || (val instanceof Array && !val.length))
                    return;
                if (val instanceof Array)
                    clst = clst.where(keys[i]).anyOf(val);
                else {  //log(777);
                    clst = (i > 0?
                        clst.filter(itm => {    //log('filter_in:');
                            return itm[keys[i]] == val;
                        }):
                        clst.where(keys[i]).equals(val));
                }
            }
            clst.toArray().then(function(lst) { log('fetch_done:'+lst.length);
                if (cb)
                    return cb(lst);
            });
        });
/*                return this;
        var transaction = this._body.transaction([table], 'readonly');
        var store = transaction.objectStore(table);
        var key = Object.keys(filter);
        store.index(key[0]).get(filter[key[0]]).onsuccess = function(e) {
            log('fetch_GOT:'+JSON.stringify(e.target.result));
            if (cb)
                cb(e.target.result);
        }*/
        return this;
    };
    this.get = function(table, query) {

        var transaction = this._body.transaction([table], "readwrite");
        var store = transaction.objectStore(table);
        var arr = [];
        var _this = this;
        store.openCursor().onsuccess = function (e) {
            var cursor = e.target.result;
            if (cursor) {
                console.dir(cursor.value);
                arr.push(cursor.value);//{req: cursor.value.req, res: cursor.value.res});
                cursor.continue();
            }
            else {  lo('get_OUT:' + arr.length);
                if (_this._then)
                    _this._then.call(_this, arr);
                _this._then = null;
                _this._ifyes = null;
            }
        }
        return this;
    }
    this.export = function (table) {

        var transaction = this._body.transaction([table], "readwrite");
        var store = transaction.objectStore(table);
        var arr = [];
        store.openCursor().onsuccess = function (e) {
            var cursor = e.target.result;
            if (cursor) {
                console.dir(cursor.value);
                arr.push({req: cursor.value.req, res: cursor.value.res});
//                arr.push(cursor.value.req);
//                arr.push(cursor.value.res);
                //                lo('cursor:'+arr.length);
                cursor.continue();
            }
            else {
                lo('export_OUT:' + arr.length);
                var dt = new Date();
                var mm = dt.getMonth() + 1;
                var dd = dt.getDate();

                fdown('idb_' + [dt.getFullYear(), !mm[1] && '0', mm, !dd[1] && '0', dd].join('') + '.back', JSON.stringify(arr));
            }
        }
        return this;
    }
    this.import = function (data) {

        data = JSON.parse(data);
        return this;
    }
    this.exists = function (table, data, cbYES, cbNO) {  log('db.exists:'+data.req);
                //console.log(data);
        var transaction = this._body.transaction([table], "readwrite");
        var store = transaction.objectStore(table);
        var request = store.index('Request').get(data.req);
        var _this = this;
        request.onsuccess = function () {   lo('exists_got_OK:'+data.req+"::"+data.uid);
            console.dir(request.result);
            console.dir(data);
            var ares;
            if (request.result) {
//* filter user
                ares = request.result;
                        log('exists_got_OK2:'+(ares instanceof Array));
                if (!(ares instanceof Array)) {   //log('exists_toarr:');
                    ares = [ares];
                        //log('exists_toarr2:'+ares.length);
                }
                        log('exists_got_OK3:'+ares.length);
                        console.dir(ares);
                data.miss = (data.miss || 0);
                for (var i = 0; i < ares.length; i++) {
                    if (ares[i].uid != data.uid || ares[i].miss != data.miss) {
                        ares.splice(i, 1);
                        i--;
                    }
                }
                //log('exists_filtered:'+ares.length);
                console.dir(ares);
            }
            if (ares && ares.length) {
                lo('exists_YES:'+data.req);
                _this._out = ares[0];
                console.dir(_this._out);
                if (cbYES)
                    cbYES.call(_this, _this._out);
//                if (_this._ifyes)
//                    _this._ifyes.call(_this);
            }
            else {
                lo('exists_NO:'+data.req);
                if (cbNO)
                    cbNO.call(_this);
//                if (_this._ifno)
//                    _this._ifno.call(_this);
            }
            _this._ifyes = _this.ifno = null;
        }
        request.onerror = function () {
            lo('exists_FAIL:');
        }
        return this;
    }
    this.then = function (fun) {
        this._then = fun;
        this._ifyes = fun;
        return this;
    }
    this.else = function (fun) {
        this._ifno = fun;
        return this;
    }
    this.onupgrade = function (fun) {
        this._onupgrade = fun;
        return this;
    }
    this.ondone = function(arg) {   //log('ondone:'+(this._then));
        if (this._then)
            this._then.call(this);
        this._then = null;
        this._ifyes = null;
    }
}
