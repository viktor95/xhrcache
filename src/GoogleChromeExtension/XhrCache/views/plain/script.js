/**
 * Created by X on 11/9/2016.
 */
DBG = 1;
console.log('= BLANK:' + document.location.href);

var _curReq;

_tabName = 'BLANK';
send('CACHE_GET', null, function(res) { log('CACHE_GET.res:'+JSON.stringify(res));
    $('#DomainFilter INPUT').val(res.domain);
    $('#XhrFilter INPUT').val(res.request);
});

(function() {
})();

function setUp() {

    $('#BFilter').on('click', function() {
       send('FILTER_UP', {domain: $('#DomainFilter INPUT').val(), request: $('#XhrFilter INPUT').val()});
    });
    $('.row_req').on('click', function() {  log('click:' + $(this).attr('id'));//+"::"+$(this).attr('datares'));
        $('#RowRes').removeClass('touched');
        $('.row_req').removeClass('sel');
        $(this).addClass('sel');
        _curReq = {id: $(this)[0].id, req: $(this).children('.txt_inner').html()};
        $('#CntRes').html('<plaintext>'+$(this).attr('datares'));
    });
    $('#CntRes').on('keydown', function() {
        $('#RowRes').addClass('touched');
    });
    $('#RowRes .save').on('click', function() {
        var jnew = $('#CntRes').html().replace(new RegExp('<plaintext>', 'g'), '').replace(new RegExp('</plaintext>', 'g'), '');
//        log('click:'+jnew+"::"+_curReq);
        _curReq.res = jnew;
        send('SAVE_CACHE_EDIT', { 'req': _curReq.req, 'res': jnew });
    });
    $('#IUp').on('click', function() {
        send('CACHE_EXPORT');
    });
    $('#IDown').on('click', function() {
//* get file
        $('#FakeInp').detach();
        var eInp = $('<input id="FakeInp" type="file" style="display:none;">');
        $('BODY').append(eInp).promise().done(function () {
            $('#FakeInp').on('change', function () {
                if (!$('#FakeInp')[0].files.length)
                    return;
                var file = $('#FakeInp')[0].files[0];
                if (!file.name.match(/idb_/)) {
                    alert('INVALID_FILE');
                    return;
                }
                log('TO_READ:');
                var reader = new FileReader();
                reader.onload = function() {    log(reader.result.length);
                    var data = JSON.parse(reader.result);
                    send('CACHE_IMPORT', data);
                };
                reader.readAsText(file);
            });
            $('#FakeInp').click();
        });
    });
}

function saveFile(name, type, data) {   lo('saveFile:' + name);

    var a = $("<a style='display:none;'/>");
//    var a = $("<a style='position:fixed;top:100px;left:100px;'>DOWN</a>");
    a.attr("download", name);
    var url = window.URL.createObjectURL(new Blob([data], { type: type }));
    a.attr("href", url);
    $("body").append(a).promise().done(function () {
        lo('LINK_READY:');
        a[0].click();
        a.remove();
    });
}

function react(msg) {

    switch(msg.subject) {
    case 'CACHE_EXPORT':
        console.dir(msg);
        var dt = new Date();
        var mm = dt.getMonth() + 1;
        var dd = dt.getDate();
        saveFile('idb_' + [dt.getFullYear(), !mm[1] && '0', mm, !dd[1] && '0', dd].join('') + '.back', "data:attachment/text", JSON.stringify(msg.body));
//        fdown('idb_' + [dt.getFullYear(), !mm[1] && '0', mm, !dd[1] && '0', dd].join('') + '.back', );
        break;
    case 'SAVE_CACHE_EDIT':
        log('react.SAVE_CACHE_EDIT:'+_curReq);
        $('#' + _curReq.id).attr('datares', _curReq.res);
        break;
    case 'CACHE_GET':   log('react.' + msg.subject+"::"+_scope.aRequest);
        console.dir(msg);
        var cnt = '';
        _scope.$apply(function() {
            _scope.aRequest = msg.body;
        });
/*        for (var i in msg.body) {
            cnt += '<div id="ReqRow_'+i+'" class="row_req"><div class="txt_inner">'+msg.body[i].req+'</div></div>';
        }
        $('#ListReq').append(cnt).promise().done(function() {
            setUp();
            var i = 0;
            $('.row_req').each(function() {
                if (!msg.body[i])
                    return;
                $(this).attr('datares', msg.body[i].res);
                i++;
            });
        });*/
        break;
    case 'CACHE_IMPORT':
        send('CACHE_GET', null);
        break;
    }
}
