'use strict';
var DBG = 1;

console.log('= APP:');

var _tabName = 'BLANK';

(function () {
  // Declare app level module which depends on views, and components
  angular.module('wireMockUI', [
    'ngRoute',
    'controllers',
    'services',
    'configuration'
    ,'ui.ace'
  ])
    .config(['$routeProvider', function ($routeProvider) {
      $routeProvider.when('/mappings', {
        templateUrl: 'mappings/mappings.html',
        controller: 'mappingsController'
      });
    }]).
    config(['$locationProvider', '$routeProvider', function ($locationProvider, $routeProvider) {
      $routeProvider.otherwise({ redirectTo: '/mappings' });
    }]);

  angular.module('configuration', []);
  angular.module('controllers', ['services']);
  angular.module('services', ['configuration']);
})();