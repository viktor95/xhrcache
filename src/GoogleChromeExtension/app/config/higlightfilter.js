angular.module('services', [])
    .filter('highlight', function ($sce) {
        return function (text, phrase) {
            if (!text)
                return;

            if (phrase) text = text.replace(new RegExp('(' + phrase + ')', 'gi'),
                '<span class="highlighted">$1</span>')

            return $sce.trustAsHtml(text)
        }
    })