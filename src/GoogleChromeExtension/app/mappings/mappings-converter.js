(function () {
    angular.module('services').factory('mappingsConverterService', function () {
        var convertToMapping = function(data)
        {

            var grouped = Lazy(data).where(function (item) {
                    return item.request.method != 'OPTIONS';
                }).map(function (item) {

                    item.request.urlShort = item.request.url.substr(item.request.url.lastIndexOf('/') + 1);

                    if (item.request.urlShort.includes('?'))
                        item.request.urlShort = item.request.urlShort.substr(0, item.request.urlShort.indexOf('?'));

                    try {
                        JSON.parse(item.response.body)
                        item.response.isJson = true;
                    } catch(e ) {
                        item.response.isJson = false;
                    }
                    if (!item.request.method)
                        item.request.method = "GET";

                    if (!item.request.dateLastHit)
                        item.request.dateLastHit = new Date();

                    return item;
                }).groupBy(function (item) {

                    try {
                        return item.request.headers.Authorization.equalTo
                    }
                    catch (e) {
                        return "No user ID";
                    }
                }).toArray();
                
                var select = Lazy(grouped).map(function (i) {
                    return {
                        userId: i[0],
                        mappings: i[1]
                    }
                }).toArray();

                return select;

        }
        
        return {
            convertToMapping : convertToMapping
        }
    })
})();