(function () {
    angular.module('services').factory('wiremockService', function ($http, $q, endpoints, mappingsConverterService, $interval) {

        var mappingsPromise = null;
        var missCallsPromise = null;
        var cacheChangeCallbacks = [];

        var commandPromiseDictionary = {}

        chrome.runtime.onMessage.addListener(function (msg, sender, sendResponse) { //log('mappings.onMessage:'+msg.subject);
            console.dir(msg.body);
            switch (msg.subject) {
                case 'CACHE_GET':
                    if (!mappingsPromise) {
                        cacheChangeCallbacks.forEach(function (callback) {
                            callback.call();
                        }, this);
                        return;
                    }
                    if (msg.body && mappingsPromise) {
                        var data = mappingsConverterService.convertToMapping(msg.body);
                        mappingsPromise.resolve(data);
                        mappingsPromise = null;
                        return;
                    }
                    break;
                case 'missed_cache_get_message_subject':
                    if (msg.body && missCallsPromise) {
                        missCallsPromise.resolve({data: {requests: msg.body}});
                        missCallsPromise = null;
                        return;
                    }
                    break;
                case 'ITEM_DEL':
                case 'SAVE_CACHE_EDIT':
                    if (commandPromiseDictionary[msg.subject]) {
                        commandPromiseDictionary[msg.subject].resolve();
                        delete commandPromiseDictionary[msg.subject];
                    }
                    break;
            }
        });

        var sendCommand = function (subject, body, waitForResponse) {

            var defer = $q.defer();
            send(subject, body);

            waitForResponse = waitForResponse ? true : false;

            if (!waitForResponse) {
                defer.resolve();
                return defer.promise;
            }
            commandPromiseDictionary[subject] = defer;
            return defer.promise;
        }

        var onCacheChange = function (callback) {
            cacheChangeCallbacks.push(callback);
        }

        var getMappings = function () {
            mappingsPromise = $q.defer();
            send('CACHE_GET', null);
            return mappingsPromise.promise;
        }

        var getMissedRequests = function () {
            missCallsPromise = $q.defer();
            send('missed_cache_get_message_subject', null);
            return missCallsPromise.promise;
        }

        var save = function (mapping) {
            console.dir(mapping);
            return sendCommand("SAVE_CACHE_EDIT", mapping);
        }

        var deleteMapping = function (mapping) {
            return sendCommand("ITEM_DEL", { req: mapping.request.url }, false);
        }

        var addNewFromMissedRequest = function (missedRequest, selectedUserId) {
            var model = {
                request: {
                    method: missedRequest.method,
                    url: missedRequest.url,
                    headers: {
                        Authorization: { equalTo: selectedUserId }
                    }
                },
                response: {
                    status: 200,
                    body: "{ \"new\" : true}"
                }
            }

            return save(model);
        }

        return {
            getMappings: getMappings,
            getMissedRequests: getMissedRequests,
            save: save,
            addNewFromMissedRequest: addNewFromMissedRequest,
            deleteMapping: deleteMapping,
            onCacheChange: onCacheChange
        }

    });
})();

function react(msg) {

    switch (msg.subject) {
        case 'CACHE_GET':
            items = msg.body;
            log('r.CACHE_GET:' + items);
            break;
    }
}