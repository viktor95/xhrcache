'use strict';
(function () {
  angular.module('controllers')
    .controller('mappingsController', function ($scope, wiremockService, $interval) {

      $scope.selected = {};
      $scope.selectedMappings = [];
      $scope.refreshAutomaticaly = true;
      $scope.requetsHeadersEditor = {};
      $scope.responseBodyEditor = {};
      $scope.grouppedMappingsByUserId = [];

      var stopRefreshing = {};
      var changeSubject =new Rx.Subject();

      changeSubject.debounce(500).subscribe(function(mapping){
        wiremockService.save(mapping);
      });

      $scope.updateMappings = function () {
        return wiremockService.getMappings().then(function (mappings) {
          $scope.grouppedMappingsByUserId = mappings;
          
          if (mappings.length > 0 && !$scope.selected.userId) {
            $scope.selected.userId = mappings[0].userId;
          }
          $scope.filerMappingsBySelection();
        });
      };

      $scope.updateMappings();
      wiremockService.onCacheChange(function()
      {
        $scope.updateMappings();
      });

      $scope.requestHeadersEditorLoaded = function (e) {
        $scope.requetsHeadersEditor = e;
        $scope.requetsHeadersEditor.$blockScrolling = Infinity;
      }

      $scope.responseBodyEditorLoaded = function (e) {

        $scope.responseBodyEditor = e;
        $scope.responseBodyEditor.$blockScrolling = Infinity;
      }

      $scope.responseBodyEditorChange = function (e) {  
        if ($scope.responseBodyEditor.isFocused() && $scope.selected.mapping) {
          $scope.selected.mapping.response.body = $scope.responseBodyEditor.getValue();
          changeSubject.onNext($scope.selected.mapping); 
        }
      }

      $scope.deleteMapping = function (event, mapping) {

        event.stopImmediatePropagation();

        if (mapping == $scope.selected.mapping) {
          $scope.selectMapping(null);
        }

        wiremockService.deleteMapping(mapping).then(function () {
          $scope.updateMappings();
        });
      }

      $scope.addNewOne = function (e) {
        wiremockService.addNewFromMissedRequest(e, $scope.selected.userId).then(function () {
          $scope.refreshMissedRequests();
          $scope.updateMappings().then(function () {
            
            var addedItem = Lazy($scope.selectedMappings).where(function(m) {return m.request.url === e.url}).first();
            $scope.selectMapping(addedItem);  
            //$scope.selected.mapping = addedItem;//$scope.selectedMappings[$scope.selectedMappings.length - 1];
          });
        });
      }

      $scope.changeRefresh = function (refresh) {
        if (refresh) {
          stopRefreshing = $interval(function () {
            $scope.refreshMissedRequests();
          }, 1000);
        }
        else {
          if (angular.isDefined(stopRefreshing)) {
            $interval.cancel(stopRefreshing);
            stopRefreshing = undefined;
          }
        }
      }

      $scope.requestHeadersChange = function (e, b) {
        if ($scope.requetsHeadersEditor.isFocused() && $scope.selected.mapping) {
          $scope.selected.mapping.request.headers = JSON.parse($scope.requetsHeadersEditor.getValue());
          changeSubject.onNext($scope.selected.mapping); 
        }
      }

      $scope.refreshMissedRequests = function () {
        wiremockService.getMissedRequests().then(function (requests) {
          $scope.missedRequests = requests.data.requests;
          $scope.missedRequestsUpdatedAt = new Date();
        });
      }

      $scope.refreshMissedRequests();

      $scope.mapingsFilterChanged = function (fulltext) {
        $scope.filerMappingsBySelection();
      }

      $scope.filerMappingsBySelection = function () {
        if ($scope.grouppedMappingsByUserId.length === 0)
          return;

        var mappings = Lazy($scope.grouppedMappingsByUserId).where(function (item) {
          return item.userId == $scope.selected.userId
        }).toArray()[0].mappings;

        $scope.selectedMappings = Lazy(mappings).where(function (item) {
          if (!$scope.selected.mappingsFiler) return true;
          return JSON.stringify(item.request).toLowerCase().includes($scope.selected.mappingsFiler.toLowerCase())
            || JSON.stringify(item.response).toLowerCase().includes($scope.selected.mappingsFiler.toLowerCase())
        }).toArray();

        if ($scope.selectedMappings.length == 0)
          $scope.selectMapping(null);
        else if ($scope.selectedMappings.length == 1) {
          $scope.selectMapping($scope.selectedMappings[0]);
        }
      }

      $scope.removeSearch = function () {
        $scope.selected.mappingsFiler = "";
        $scope.filerMappingsBySelection();
      }

      $scope.selectMapping = function (item) {

        var mappings = Lazy($scope.grouppedMappingsByUserId).where(function (item) {
          return item.userId == $scope.selected.userId
        }).toArray()[0].mappings;

        angular.forEach(mappings, function (item2, index) {
          item2.isSelected = false;
        });

        $scope.selected.mapping = item;

        if (item) {
          $scope.selected.mapping.isSelected = true;

          if ($scope.selected.mapping.request.headers)
            $scope.requetsHeadersEditor.getSession().setValue(JSON.stringify($scope.selected.mapping.request.headers, null, 2))
          else
            $scope.requetsHeadersEditor.getSession().setValue('');

          try {
            var body = JSON.stringify(JSON.parse($scope.selected.mapping.response.body), null, 2);
            $scope.responseBodyEditor.getSession().setValue(body);
            $scope.bodyParsingFailed = false;

          } catch (e) {
            $scope.bodyParsingFailed = true;

          }
        }
        else {
          $scope.responseBodyEditor.getSession().setValue('');
          $scope.requetsHeadersEditor.getSession().setValue('');
        }
      }
    });

})();
