/**
 * Created by X on 11/10/2016.
 */
DBG = 1;
log('- SOCKET_BG:');

function send2tab(tab, subj, body) {   log('socketBG:'+subj);

    if (!tab) {
        for (var i in _atab) {
            chrome.tabs.sendMessage(_atab[i], {
                subject: subj,
                body: body,
            });
        }
        return;
    }
    chrome.tabs.sendMessage(tab, {
        subject: subj,
        body: body,
    });
}