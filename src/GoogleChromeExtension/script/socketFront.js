/**
 * Created by X on 11/9/2016.
 */
var DBG = 1;

function log(msg) {
    console.log(msg);
}
log('= SOCKET_FRONT:');

var _pending = {};
var _tabName;

function send(subj, body, cb) { log('socketFront.send:'+subj);

//    _pending.cb = cb;
    chrome.runtime.sendMessage({
        subject: subj,
        body: body,
        tab: _tabName
    }, function (res) {
        if (cb)
            cb.call(null, res);
    });
}

function receive(msg) { console.log('= socketFront.receive:'+msg.subject);//+JSON.stringify(msg));

    switch (msg.subject) {
    default:
        return react(msg);
    }
}

chrome.runtime.onMessage.addListener(function (msg, from, sendResp) {

    if (msg.tab == _tabName)
        return;
    receive(msg);
//    console.log('msg_got:' + from);//_tabName+"::"+JSON.stringify(msg));
});
