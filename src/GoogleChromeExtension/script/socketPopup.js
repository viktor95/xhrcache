/**
 * Created by X on 11/10/2016.
 */
DBG = 1;
log('- SOCKET_POPUP:');

var _tabName = 'POPUP';

function send(subj, body, cb) {   log('socketPopup:'+subj);

    chrome.runtime.sendMessage({
        subject: subj,
        body: body,
        tab: _tabName
    }, function(res) {
        if (cb)
            cb.call(null, res);
    });
}