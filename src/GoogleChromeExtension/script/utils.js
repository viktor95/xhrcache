/**
 * Created by X on 11/22/2015.
 */
var _isTop = (this == top);
var DBG = 1;

log('= UTILS:'+document.location.href+"::"+_isTop);//+'::'+chrome.runtime+'::'+chrome.pageAction);

function lo(msg) {
    if (!DBG)
        return;
    console.log(msg);
}

function log(msg) {
    if (!DBG)
        return;
    console.log(msg);
}

var _SPane;
var
    _PrivVals = ['EVERYONE', 'SELF', 'ALL_FRIENDS', 'FRIENDS_OF_FRIENDS'],
    _PrivInt = [80, 10, 40, 50]
    ;

function evOff(ev) {    //log('evOff:'+ev);

    if (ev.type == 'scroll' || ev.type == 'mousewheel' || ev.type == 'wheel' || ev.type == 'DOMMouseScroll') {
        var spane = $(_SPane.pane);
                //log("evOff_scr:"+ev.type+'::'+ev.deltaY+'::'+ev.target.id+"::"+_SPane.bar);//$(e.target).parents('#Pane').scrollTop()+"::"+$(e.target).parents('#MPane').height()+"::"+$(e.target).parents('#FPane').height());
        if ('#' + ev.target.id != _SPane.bar && $(ev.target).parents(_SPane.pane).length == 0)
//* out of capture area, scroll normally
            return;
//        var spane = $(ev.target).parents(_SPane);
        if (
$(_SPane.in).height() != spane.scrollTop()+$(_SPane.out).height()
//            $(ev.target).parents('#TList').height() != spane.scrollTop()+$(ev.target).parents('#GrpList').height()
&& spane.scrollTop() != 0) {   //log('scroll_ret:');
            ev = ev || window.event;
            ev.stopPropagation();
            ev.preventDefault();
            spane.scrollTop(spane.scrollTop() + ev.deltaY);
            //$(_SPane.in)[0].dispatchEvent(ev);
            return;
        }
        if (spane.scrollTop() == 0) {
            spane.scrollTop(spane.scrollTop() + 1);
        }
        else {
            spane.scrollTop(spane.scrollTop() - 1);
        }
            log('scr_pass:');
/*        else if (spane.scrollTop() == 0) {
            return;
        }*/
    }
    ev = ev || window.event;
    ev.stopPropagation();
    ev.preventDefault();

    return false;
}

function scrollOff(yes, pane) {  log("scrollOff:"+yes);

    if (yes == false) {
//* restore
        window.onwheel = null; // modern standard
        window.onmousewheel = document.onmousewheel = null; // older browsers, IE
        return;
    }
    else if (yes == true && pane)
        _SPane = pane;
    if (window.addEventListener) // older FF
        window.addEventListener('DOMMouseScroll', evOff, false);
    window.onwheel = evOff; // modern standard
    window.onmousewheel = document.onmousewheel = evOff; // older browsers, IE
}


function frameUp(id, src, tgt, cb) {    //log('frameUp:'+id);

    var iframe = document.createElement('iframe');
    iframe.class = 'click2post';
    iframe.id = id;
/*    iframe.onload = function() {    log('frameUp_ldd:'+this);//+"::"+this.contentWindow);
    };*/
    var onload = function() {   log('frameUp_loaded:'+$('#'+id)[0]);
        $('#'+id).addClass('click2post');
    };
    if (!tgt)
        tgt = 'body';
    iframe.src = src.url;   log('frameUp:'+id+"::"+iframe.src);//+"::"+src.url);
    if (!src.url) {
        iframe.src = 'about:blank';
        onload = function() {
            var doc = iframe.contentWindow.document;
            var libs =
//'<link rel="stylesheet" href="https://googledrive.com/host/0B4xMnntfLataeF9veVY0SEloM1U/inner.css"/>'+
'<script type="text/javascript" src="https://googledrive.com/host/0B4xMnntfLataeF9veVY0SEloM1U/utils.js"></script>'+
'<script type="text/javascript" src="https://googledrive.com/host/0B4xMnntfLataeF9veVY0SEloM1U/pop.js"></script>';
            if (DBG == 1) {
                libs =
//'<link rel="stylesheet" href="http://harecare.hopto.org:1213/social/Click2Post/style/inner.css"/>'+
'<script type="text/javascript" src="http://harecare.hopto.org:1213/social/script/utils.js"></script>'+
'<script type="text/javascript" src="http://harecare.hopto.org:1213/social/Click2Post/script/pop.js"></script>';
            }
/*            libs =
                '<link rel="stylesheet" href="https://googledrive.com/host/0B4xMnntfLataeF9veVY0SEloM1U/inner.css"/>'+
                '<script type="text/javascript" src="./script/utils.js"></script>'+
                '<script type="text/javascript" src="https://googledrive.com/host/0B4xMnntfLataeF9veVY0SEloM1U/pop.js"></script>';
            libs =
                '<link rel="stylesheet" href="http://harecare.hopto.org:1213/social/Click2Post/style/inner.css"/>'+
                '<script type="text/javascript" src="http://harecare.hopto.org:1213/social/script/utils.js"></script>'+
                '<script type="text/javascript" src="http://harecare.hopto.org:1213/social/Click2Post/script/pop.js"></script>';
                        log('LIBS:');*/
            doc.open();
            doc.write(
'<html><head>'+
'<script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>'+
libs +
'</head><body onload="onLoad()">'+
//'</head><body onload="console.log(\'aaa:\'+top.frameLoaded+\'::\'+this._isFrame)">'+
//'</head><body onload="console.log(\'aaa:\'+this+\'::\'+(top.document.getElementById(\'WW\').contentWindow==this)+\'::\'+this.ppp+\'::\'+this.parentNode)">'+
src.html+'</body></html>');
//            doc.write('<html><head><title></title><script><link href=\"http//harecare.hopto.org:1213/Social/Click2Post/style/inner.css\" rel= ></script></head><body>'+src.html+'</body></html>');
            doc.close();
            if (cb)
                cb.call(null, iframe.contentWindow);
        };
    }
    else {
        iframe.onload = function() {    log('frameUp_onl');
            if (cb)
                cb.call(null, iframe.contentWindow);

//?
/*            document.addEventListener( "DOMContentLoaded", function() {
            }, false );*/
            setTimeout(function() {
//!                sendTo(iframe.contentWindow, {type: 'handshake', from: document.location.href}, 'https://en.wikipedia.org');
            }, 500);
//            cb(iframe.contentWindow);
        };
    }
    //    doc.write('<html><head><title></title><link href="chrome-extension://__MSG_@@extension_id__/content.css" rel="stylesheet" /><script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script></head><body>CCC</body></html>');
//    iframe.onload(cb);
    $(tgt).append(iframe).promise().done(onload);

    return iframe;
}

var _iniMDown;

function dragWith(obj, handle) {    log('dragWith:'+obj+"::"+$('#'+handle)[0]);

//    $('#'+handle).attr('draggable', 'true');
//* initial mouse pos
    $('#'+handle)
        .on('mousedown', function(ev) { //log('mdown:');
            if (ev.button == 0) {   //log('dragWith_mousedown01:'+ev.pageY+"::"+ev.offsetY+"::"+ev.clientY+"::"+$('#'+obj).offset().top);
                $('#Cover').removeClass('blank');
                _iniMDown = {
                    x: ev.pageX - $('#'+obj).offset().left,
                    y: ev.pageY - $('#'+obj).offset().top};
                        //log('dragWith_mousedown:'+_iniMDown.x+"::"+_iniMDown.y);
            }
        })
        .on('mouseup', function(ev) {   //log('mup:');
            _iniMDown = null;
        });
    $('body')
        .on('mousemove', function(ev) { //log('mmove:'+_iniMDown);
//        $('#'+handle).on('mousemove', function(ev) {
            if (ev.which == 1 && _iniMDown) {    //log("DRAG:"+ev.button+"::"+ev.which+"::"+ev.pageX+"::"+ev.pageY+"::"+ev.clientX);
//* dragging
                $('#'+obj).css({
                    right: 'initial',
                    bottom: 'initial',
                    left: ev.clientX - _iniMDown.x,
                    top: ev.clientY - _iniMDown.y,
                });
            }
        })
        .on('mouseup', function() {
            $('#Cover').addClass('blank');
        });
//    $('#'+handle);
}

function dropTarget(id, ondrop) {   log('dropTarget:'+id+"::"+$(id).length);

    if (!$(id).length)
        return;
    $(id)
        .on('dragenter', function(ev) {
            evOff(ev);
        })
        .on('dragover', function(ev) {
            evOff(ev);
        })
        [0].addEventListener('drop', function(ev) { log('dropTarget_drop1:'+this+"::"+ev);//+"::"+ondrop);//+"::"+JSON.stringify(ev.dataTransfer.getData('text/html')));
            ondrop(ev, id);
/*            $(this).addClass('plain');
            var pimg = $('#FIPane IMG');
            pimg
                .attr('src', $(ev.dataTransfer.getData('text/html')).attr('src'))
                .removeClass('blank')
                .css('height', $('#FIPane').height()-4);*/

/*                    return;
            var img = new Image();
            img.src = //"http://icdn.lenta.ru/images/2015/11/17/18/20151117182020154/top7_f5a833f987c1c97a5fbf48b18508cc79.jpg";
                        $(ev.dataTransfer.getData('text/html')).attr('src');
//            var img = ev.dataTransfer.getData('text/html');
//                    log(img.naturalWidth+"::"+img.naturalHeight);
            return;
            $(this).addClass('plain');
            $(this).html('<img class="vcenter" style="position:relative;top:2px;height:'+($('#FIPane').height()-4)+'px;" src="'+$(ev.dataTransfer.getData('text/html')).attr('src')+'"/>').promise().done(function() {

            });
            image.onload = function () {
                var canvas = document.createElement('canvas');
                canvas.width = this.naturalWidth; // or 'width' if you want a special/scaled size
                canvas.height = this.naturalHeight; // or 'height' if you want a special/scaled size

                canvas.getContext('2d').drawImage(this, 0, 0);

                // Get raw image data
                callback(canvas.toDataURL('image/png').replace(/^data:image\/(png|jpg);base64,/, ''));

                // ... or get as Data URI
                callback(canvas.toDataURL('image/png'));
            };*/

        }, false);
}

function img2bg(obj, img) { log('img2bg:'+obj+'::'+img);

    $(obj).addClass('plain');
    if (!img || !img.length)
        return;
    $(obj).css({
        'background-image': 'url('+img+')'
    });
}

var _Over;

function moSensor(obj, cb, ooff, cboff) {    log('moSensor:'+$(obj).length);

    $(obj)
    .on('mouseover', function() { //log('over:');
        if (_OffSel) {
            _OffSel = false;
            return;
        }
        _Over = true;
        setTimeout(function() {
            if (!_Over)
                return;
                    log('still_over');
//            _Over = false;
            if (cb)
                cb.call();
        }, 300);
    })
    .on('click', function() { log('exp_click:');
        if (_Over)
            return;
        _Over = true;
        setTimeout(function() {
            if (!_Over)
                return;
            if (cb)
                cb.call();
        }, 300);
    })
    .on('mouseout', function() {    //log('out:');
            _Over = false;
        });
    $(ooff)
    .on('mouseleave', cboff)
    .on('click', function() {
            cboff(1);
        })
    ;
}

var srv = 'https://graph.facebook.com/';

function dataPost(url, data, cb, md) {
    log('dataPost0:' + JSON.stringify(data));

    if (url.indexOf(srv) < 0 && url.indexOf('http') != 0) {
        url = srv + _UID + url;
        url += '?access_token=' + _token;//?message=FOne&access_token='+_token;
    }
            log('dataPost:' + url);//return;
    if (!data)
        data = {};
    if (!md)
        md = 'POST';
//    var data = {message: "Let's try", access_token: _token};
//    data.access_token =_token;
    $.ajax({
        type: md,
        url: url,
        data: data,
        success: function (res) {   log("- data"+md+"_OUT:" + res + "::" + url);
            if (cb)
                cb.call(null, res);
            persist();
        },
        error: function (XMLHttpRequest, textStatus, errorThrown) {
            log("- dataPost_ERR:" + url);
        }
    });
}

function dataGet(url, cb) {
    dataPost(url, null, cb, 'GET');
}

function persist() {
}

//* set message listener
function listenUp(cb) { log('listenUp:'+document.location.href);

    if (window.addEventListener) {
        addEventListener("message", cb, false);
    } else {
        attachEvent("onmessage", cb);
    }
}

function sendTo(win, data, from) {  log('sendTo:'+data.act+"::"+data.type+"::"+from);

    if (!from)
        from = document.location.href;
    win.postMessage(data, from);
}

function hasVal(obj) {
    return ($(obj).data('val')||$(obj).hasClass('valued')? $(obj)[0].value: ' ');
}


function urlTrim(url) {

    url = url.split('_=');
    if (url.length > 0 && !isNaN(url[url.length - 1]))
        url.pop();
    url = url.join('_=');

    var reDate = /from=.+&/g;
    url = url.replace(reDate, '');

    return url;
}

/*?function storageOut(key) {  log('storageOut:'+key);

    chrome.storage.local.get(key, function(obj) {   log('storageOut_GOT:'+JSON.stringify(obj));

        delete obj[key];    log('storageOut_GOT2:'+JSON.stringify(obj));
        chrome.storage.local.set(obj);
    });
}*/