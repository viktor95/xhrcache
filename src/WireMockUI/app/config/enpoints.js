(function () {
    angular.module('configuration')
        .constant("endpoints", {
            'mappings': 'http://localhost:8080/__admin/mappings/',
            'missedRequests' : 'http://localhost:8080/__admin/requests/unmatched/'
        });
})();