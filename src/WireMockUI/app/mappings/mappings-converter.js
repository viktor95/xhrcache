(function () {
    angular.module('services').factory('mappingsConverterService', function () {
        var convertToMapping = function (data) {

            var grouped = Lazy(data).where(function (item) {
                return item.request.method != 'OPTIONS';
            }).map(function (item) {

                item.request.urlShort = item.request.url.substr(item.request.url.lastIndexOf('/') + 1);

                if (item.request.urlShort.includes('?'))
                    item.request.urlShort = item.request.urlShort.substr(0, item.request.urlShort.indexOf('?'));

                try {
                    JSON.parse(item.response.body)
                    item.response.isJson = true;
                } catch (e) {
                    item.response.isJson = false;
                }
                if (!item.request.method)
                    item.request.method = "GET";

                if (!item.request.dateLastHit)
                    item.request.dateLastHit = new Date(2016, 12, 19, 14, 17, 4);

                return item;
            }).groupBy(function (item) {

                try {
                    return item.request.headers.Authorization.equalTo
                }
                catch (e) {
                    return "No user ID";
                }
            }).toArray();

            var select = Lazy(grouped).map(function (i) {
                return {
                    userId: i[0],
                    mappings: i[1]
                }
            }).toArray();

            return select;
        }

        var convertToMissedRequest = function (missedCollection) {
            var groupedByMethodUrl = Lazy(Lazy(missedCollection).groupBy(function (missed) {
                return missed.method + missed.url + missed.headers.Authorization;
            }).toArray()).map(function (item) {
                return item[1][0];
            }).toArray();


            var group = Lazy(groupedByMethodUrl).map(function (missed) {
                missed.urlShort = missed.url.substr(missed.url.lastIndexOf('/') + 1);

                if (missed.urlShort.includes('?'))
                    missed.urlShort = missed.urlShort.substr(0, missed.urlShort.indexOf('?'));

                if (!missed.headers.Authorization)
                    missed.headers.Authorization = "No user ID"

                return missed;
            }).groupBy(function (item) {

                return item.headers.Authorization

            }).toArray();

            return Lazy(group).map(function (i) {
                return {
                    userId: i[0],
                    missedRequests: i[1]
                }
            }).toArray();
        }

        return {
            convertToMapping: convertToMapping,
            convertToMissedRequest: convertToMissedRequest,

        }
    })
})();