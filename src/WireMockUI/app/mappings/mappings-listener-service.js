(function () {
    angular.module('services').factory('wiremockService', function ($http, $q, endpoints, mappingsConverterService, $interval) {

        var mappingsPromise = null;
        var cacheChangeCallbacks = [];

        chrome.runtime.onMessage.addListener(function (msg, sender, sendResponse) { //log('mappings.onMessage:'+msg.subject);
            console.dir(msg.body);
            switch (msg.subject) {
                case 'CACHE_GET':
                    if (!mappingsPromise) {
                        cacheChangeCallbacks.forEach(function (callback) {
                            callback.call();
                        }, this);
                        return;
                    }
                    if (msg.body && mappingsPromise) {
                        var data = mappingsConverterService.convertToMapping(msg.body);
                        mappingsPromise.resolve(data);
                        mappingsPromise = null;
                        return;
                    }
                    break;
            }

        });

        var onCacheChange = function (callback) {
            cacheChangeCallbacks.push(callback);
        }

        var getMappings = function () {
            console.log('getMappings:');
            mappingsPromise = $q.defer();
            //            chrome.runtime.sendMessage({ subject: 'CACHE_GET', tab: 'BLANK' });
            send('CACHE_GET', null);
            return mappingsPromise.promise;
        }

        var getMissedRequests = function () {
            return $q.defer().promise;
        }

        var save = function (mapping) {
            send("SAVE_CACHE_EDIT", mapping);
            return $q.defer().promise;
        }

        var deleteMapping = function (mapping) {
            return $q.defer().promise;
        }

        var addNewFromMissedRequest = function (missedRequest, selectedUserId) {
            return $q.defer().promise;
        }

        return {
            getMappings: getMappings,
            getMissedRequests: getMissedRequests,
            save: save,
            addNewFromMissedRequest: addNewFromMissedRequest,
            deleteMapping: deleteMapping,
            onCacheChange: onCacheChange
        }

    });
})();

function react(msg) {

    switch (msg.subject) {
        case 'CACHE_GET':
            items = msg.body;
            log('r.CACHE_GET:' + items);
            break;
    }
}