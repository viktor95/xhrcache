(function () {
    angular.module('services').factory('wiremockService', function ($http, $q, endpoints, mappingsConverterService) {

        var getMappings = function () {
            var future = $q.defer();

            $http.get(endpoints.mappings).then(function (response) {
                var select = mappingsConverterService.convertToMapping(response.data.mappings);
                future.resolve(select);

            }, function (err) {
                alert('Loading mappings failed');
                future.reject();
            });

            return future.promise;
        }

        var getMissedRequests = function () {
            return $http.get(endpoints.missedRequests).then(function (reposne) {
                return mappingsConverterService.convertToMissedRequest(reposne.data.requests)
            });
        }

        var saveChanges = function (mapping) {
            var newMap = JSON.parse(JSON.stringify(mapping));

            delete newMap.request.urlShort;
            delete newMap.request.dateLastHit;
            delete newMap.response.isJson;

            var payload = {
                request: newMap.request,
                response: newMap.response
            };
            return $http.put(endpoints.mappings + mapping.id, payload);
        }

        var deleteMapping = function (mapping) {
            return $http.delete(endpoints.mappings + mapping.id);
        }

        var addNewFromMissedRequest = function (missedRequest, selectedUserId) {

            var model = {
                request: {
                    method: missedRequest.method,
                    url: missedRequest.url,
                    headers: {}
                },
                response: {
                    status: "200",
                    body: ""
                }
            }
            if (selectedUserId) {
                model.request.headers.Authorization = { equalTo: selectedUserId }
            }

            var ss = JSON.stringify(model);

            return $http.post(endpoints.mappings, ss);
        }

        var saveNew = function (item, userId) {
            if (userId && userId != "No user ID") {
                if (!item.request.headers)
                    item.request.headers = {};

                item.request.headers.Authorization = { equalTo: userId }
            }

            return $http.post(endpoints.mappings, item);
        }

        return {
            getMappings: getMappings,
            getMissedRequests: getMissedRequests,
            saveChanges: saveChanges,
            addNewFromMissedRequest: addNewFromMissedRequest,
            deleteMapping: deleteMapping,
            saveNew: saveNew
        }

    });
})();