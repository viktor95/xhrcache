'use strict';
(function () {
  angular.module('controllers')
    .controller('mappingsController', function ($scope, wiremockService, $interval, guidService) {

      $scope.selected = {};
      $scope.selectedMappings = [];
      $scope.refreshAutomaticaly = true;
      $scope.requetsHeadersEditor = {};
      $scope.responseBodyEditor = {};
      $scope.grouppedMappingsByUserId = [];
      $scope.grouppedMissingRequestsByUserId = [];
      $scope.verbs = ["GET", "POST", "PUT", "PATCH", "DELETE", "COPY", "HEAD", "LINK", "UNLINK", "PURGE", "LOCK", "UNLOCK", "PROPFIND", "VIEW",];

      var stopRefreshing = {};
      var changeSubject = new Rx.Subject();

      changeSubject.debounce(500).subscribe(function (mapping) {
        wiremockService.saveChanges(mapping).then(function () {
          toastr.success('Saved');
          $scope.updateMappings();
        });
      });

      $scope.updateMappings = function () {
        return wiremockService.getMappings().then(function (mappings) {
          $scope.grouppedMappingsByUserId = mappings;

          if (mappings.length > 0 && !$scope.selected.userId) {
            $scope.selected.userId = mappings[0].userId;
          }
          $scope.filerMappingsBySelection();
        });
      };

      $scope.updateMappings();

      $scope.requestHeadersEditorLoaded = function (e) {
        $scope.requetsHeadersEditor = e;
        $scope.requetsHeadersEditor.$blockScrolling = Infinity;
      }

      $scope.responseBodyEditorLoaded = function (e) {
        $scope.responseBodyEditor = e;
        $scope.responseBodyEditor.$blockScrolling = Infinity;
      }

      $scope.responseBodyEditorChange = function (e) {
        if ($scope.responseBodyEditor.isFocused() && $scope.selected.mapping) {
          $scope.selected.mapping.response.body = $scope.responseBodyEditor.getValue();
          changeSubject.onNext($scope.selected.mapping);
        }
      }

      $scope.saveMappingChanges = function (e) {
        changeSubject.onNext($scope.selected.mapping);
      }

      $scope.addNewMapping = function () {
        var mapping = {
          request: {
            url: '/newUrl',
            method: 'GET'
          },
          response: {
            body: "{ \"isNew\" : true}"
          }
        }
        wiremockService.saveNew(mapping, $scope.selected.userId).then(function (response) {
          var newId = response.data.id;
          $scope.updateMappings().then(function (data) {

            $scope.selectMapping(
              Lazy($scope.selectedMappings).where(function (map) {
                return newId = map.id;
              }).toArray()[0]);
          });
        });

      }

      $scope.deleteMapping = function (event, mapping) {

        event.stopImmediatePropagation();

        if (mapping == $scope.selected.mapping) {
          $scope.selectMapping(null);
        }

        wiremockService.deleteMapping(mapping).then(function () {
          $scope.updateMappings().then(function () {
            if ($scope.selectedMappings.length === 1) {
              if ($scope.grouppedMappingsByUserId.length > 0) {
                $scope.selected.userId = $scope.grouppedMappingsByUserId[0].userId;
              }
              $scope.selected.mappings = null;
              $scope.filerMappingsBySelection();
            }
          });
        });
      }

      $scope.addNewOne = function (e) {
        wiremockService.addNewFromMissedRequest(e, $scope.selected.userId).then(function () {
          $scope.updateMappings().then(function () {
            $scope.selected.mapping = $scope.selectedMappings[$scope.selectedMappings.length - 1];
          });
        });
      }

      $scope.changeRefresh = function (refresh) {
        if (refresh) {
          stopRefreshing = $interval(function () {
            $scope.refreshMissedRequests();
          }, 1000);
        }
        else {
          if (angular.isDefined(stopRefreshing)) {
            $interval.cancel(stopRefreshing);
            stopRefreshing = undefined;
          }
        }
      }

      $scope.requestHeadersChange = function (e, b) {
        if ($scope.requetsHeadersEditor.isFocused() && $scope.selected.mapping) {
          try {
            $scope.selected.mapping.request.headers = JSON.parse($scope.requetsHeadersEditor.getValue());
            changeSubject.onNext($scope.selected.mapping);
          }
          catch (e) { }

        }
      }

      $scope.refreshMissedRequests = function () {
        wiremockService.getMissedRequests().then(function (requests) {
          $scope.grouppedMissingRequestsByUserId = requests;
          $scope.filerMappingsBySelection();
        });
      }

      $scope.refreshMissedRequests();



      $scope.filerMappingsBySelection = function () {
        filterMappings();
        filterMissedRequests();
      }

      $scope.removeSearch = function () {
        $scope.selected.mappingsFiler = "";
        $scope.filerMappingsBySelection();
      }

      $scope.selectMapping = function (item) {

        var mappingsList = Lazy($scope.grouppedMappingsByUserId).where(function (item) {
          return item.userId == $scope.selected.userId
        }).toArray();

        if (mappingsList.length === 0)
          return;

        var mappings = mappingsList[0].mappings;

        angular.forEach(mappings, function (item2, index) {
          item2.isSelected = false;
        });

        $scope.selected.mapping = item;

        if (item) {
          $scope.selected.mapping.isSelected = true;

          if ($scope.selected.mapping.request.headers)
            $scope.requetsHeadersEditor.getSession().setValue(JSON.stringify($scope.selected.mapping.request.headers, null, 2))
          else
            $scope.requetsHeadersEditor.getSession().setValue('');

          try {
            var body = JSON.stringify(JSON.parse($scope.selected.mapping.response.body), null, 2);
            $scope.responseBodyEditor.getSession().setValue(body);
            $scope.bodyParsingFailed = false;
          } catch (e) {
            $scope.bodyParsingFailed = true;
            $scope.responseBodyEditor.getSession().setValue($scope.selected.mapping.response.body);
          }
        }
        else {
          $scope.responseBodyEditor.getSession().setValue('');
          $scope.requetsHeadersEditor.getSession().setValue('');
        }
      }

      function filterMappings() {
        if ($scope.grouppedMappingsByUserId.length === 0) {
          $scope.selectedMappings = [];
          return;
        }

        var mappingsList = Lazy($scope.grouppedMappingsByUserId).where(function (item) {
          return item.userId == $scope.selected.userId
        }).toArray();

        if (mappingsList.length === 0)
          return;

        var mappings = mappingsList[0].mappings;

        $scope.selectedMappings = Lazy(mappings).where(function (item) {
          if (!$scope.selected.mappingsFiler) return true;
          return JSON.stringify(item.request).toLowerCase().includes($scope.selected.mappingsFiler.toLowerCase())
            || JSON.stringify(item.response).toLowerCase().includes($scope.selected.mappingsFiler.toLowerCase())
        }).toArray();

        if ($scope.selectedMappings.length == 0)
          $scope.selectMapping(null);
        else if ($scope.selectedMappings.length == 1) {
          $scope.selectMapping($scope.selectedMappings[0]);
        }
      }

      function filterMissedRequests() {
        $scope.selectedMissedRequests = [];

        if ($scope.grouppedMissingRequestsByUserId.length === 0) {
          return;
        }

        var mappingsList = Lazy($scope.grouppedMissingRequestsByUserId).where(function (item) {
          return item.userId == $scope.selected.userId
        }).toArray();

        if (mappingsList.length === 0)
          return;

        var mappings = mappingsList[0].missedRequests;

        $scope.selectedMissedRequests = Lazy(mappings).where(function (item) {
          if (!$scope.selected.mappingsFiler) return true;
          return JSON.stringify(item).toLowerCase().includes($scope.selected.mappingsFiler.toLowerCase());
        }).toArray();
      }
    });

})();
